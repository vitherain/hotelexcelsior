
var webpack = require('webpack');
var path = require('path');
var backendServerUrl = 'http://localhost:63985';

var isDevMode = process.argv.indexOf('--webdev') !== -1;
console.log('isDevMode: ' + isDevMode);

var config = {
    entry: getEntryPoints(isDevMode),
    output: {
        path: path.join(__dirname, 'bundle'),
        filename: 'bundle.js',
        publicPath: 'bundle/'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: "json-loader",
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.less$/,
                loader: 'style!css!less'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?minetype=application/font-woff&name=[name]-[hash].[ext]"
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?name=[name]-[hash].[ext]"
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader: 'url?limit=10000!img?progressive=true'
            }
        ]
    },
    devServer: {
        hot: true,
        proxy: {
            '/api/*': {
                target: backendServerUrl,
                secure: false
            }
        }
    },
    plugins: []
};

if (isDevMode) {
    // webpack-dev-server hot swapping
    config.entry.unshift('webpack/hot/dev-server');
    config.entry.unshift('webpack-dev-server/client?http://localhost:8088/');
    config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;


function getEntryPoints(isDevMode) {
    var entryPoints = [
        './Scripts/config/loadDependencies.js'
    ];

    if (isDevMode) {
        entryPoints.push('./Scripts/config/httpStubsConfigDev.js');
    } else {
        entryPoints.push('./Scripts/config/httpStubsConfigNonDev.js');
    }

    entryPoints.push('./Scripts/config/app.js');

    return entryPoints;
}