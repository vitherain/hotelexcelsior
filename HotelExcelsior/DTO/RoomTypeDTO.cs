﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelExcelsior.DTO
{
    public class RoomTypeDto : BaseDto
    {
        public string Description { get; set; }
        public int? DoubleBedsCount { get; set; }
        public int? SingleBedsCount { get; set; }
    }
}