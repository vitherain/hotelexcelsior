﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelExcelsior.DTO
{
    public class ReservationDto : BaseDto
    {
        public DateTime? DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public GuestDto Guest { get; set; }
        public RoomDto Room { get; set; }
        public WayOfReservationDto WayOfReservation { get; set; }
        public bool HasAccommodation { get; set; }
    }
}