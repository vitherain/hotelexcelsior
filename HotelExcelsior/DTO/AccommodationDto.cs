﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelExcelsior.DTO
{
    public class AccommodationDto : BaseDto
    {
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public ReservationDto Reservation { get; set; }
    }
}