﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelExcelsior.DTO
{
    public class WayOfReservationDto : BaseDto
    {
        public string Name { get; set; }
    }
}