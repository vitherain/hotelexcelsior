﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelExcelsior.DTO
{
    public class ReservationForm : BaseDto
    {
        public DateTime? DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public long GuestId { get; set; }
        public long RoomId { get; set; }
        public long WayOfReservationId { get; set; }
    }
}