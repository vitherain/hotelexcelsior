﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelExcelsior.DTO
{
    public class RoomDto : BaseDto
    {
        public string Name { get; set; }
        public bool? SmokingAllowed { get; set; }
        public RoomTypeDto RoomType { get; set; }
    }
}