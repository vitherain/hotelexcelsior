module.exports = function ($scope, $rootScope, $location, $state, authService, flashService) {

    $scope.credentials = {};

    $scope.login = function() {
        authService.authenticate($scope.credentials).then(function(user) {
            authService.setUser(user);
            $location.path("/");
        }).catch(function() {
            authService.resetUser();
            $state.go("app.static.login");
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange('Během přihlašování došlo k chybě. Zkuste to prosím znovu.');
        });
    };
};