module.exports = function($rootScope, $q, httpFacade, localStorageService, constants, $location) {

    var _authenticate = function(credentials) {
        var deferred = $q.defer();

        var storedUser = localStorageService.get(constants.currentUser) || undefined;

        if (storedUser) {
            deferred.resolve(storedUser);
        } else {
            var headers = credentials ? {authorization : "Basic "
                + btoa(credentials.username + ":" + credentials.password)}
                : {};

            httpFacade.authenticate(headers).then(function(response) {
                if (response.data) {
                    deferred.resolve(response.data);
                } else {
                    deferred.reject();
                }
            }).catch(function() {
                deferred.reject();
            });
        }

        return deferred.promise;
    };

    var _logout = function() {
        return httpFacade.logout().then(function() {
            _resetUser();
            $location.path("/");
        });
    };

    var _setUser = function(user) {
        localStorageService.add(constants.currentUser, user);
        $rootScope.user = user;
        $rootScope.authenticated = true;

        var storedSelectedRole = localStorageService.get(constants.selectedRole) || undefined;

        if (storedSelectedRole) {
            _setSelectedRole(storedSelectedRole);
        } else {
            _setSelectedRole(user.roles[0]);
            localStorageService.add(constants.selectedRole, user.roles[0]);
        }
    };

    var _resetUser = function() {
        localStorageService.remove(constants.currentUser);
        localStorageService.remove(constants.selectedRole);
        $rootScope.authenticated = false;
        _setSelectedRole(null);
    };

    var _setSelectedRole = function(selectedRole) {
        $rootScope.selectedRole = selectedRole;
        localStorageService.add(constants.selectedRole, selectedRole);
    };

    return {
        authenticate: _authenticate,
        logout: _logout,
        setUser: _setUser,
        resetUser: _resetUser,
        setSelectedRole: _setSelectedRole
    }
};