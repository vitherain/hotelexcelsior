﻿module.exports = function (httpFacade, RoomTypeDTO) {

    var _readAll = function () {
        return httpFacade.readAllRoomTypes().then(function (response) {
            return response.data.map(function (roomType) {
                return new RoomTypeDTO(roomType);
            });
        });
    };

    var _readOne = function (roomTypeId) {
        return httpFacade.readRoomType(roomTypeId).then(function (response) {
            return new RoomTypeDTO(response.data);
        });
    };

    var _save = function (roomType) {
        if (roomType.id) {
            return httpFacade.updateRoomType(roomType.id, roomType);
        } else {
            return httpFacade.createRoomType(roomType);
        }
    };

    return {
        readAll: _readAll,
        readOne: _readOne,
        save: _save
    };
};