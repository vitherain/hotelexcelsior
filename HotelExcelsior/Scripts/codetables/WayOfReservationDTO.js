﻿module.exports = function () {

    function WayOfReservationDTO(wORArg) {
        this.id = wORArg.Id ? wORArg.Id : undefined;
        this.name = wORArg.Name ? wORArg.Name : undefined;
    }

    return WayOfReservationDTO;
};