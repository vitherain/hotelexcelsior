﻿module.exports = function ($scope, $rootScope, $state, roomTypesService, constants, roomType, formUtilsService, flashService) {

    if (roomType === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst způsob rezervace");
    }

    $scope.roomType = roomType;
    $scope.isNew = roomType.id ? false : true;

    $scope.submitForm = function () {
        if ($scope.roomTypeForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.roomTypeForm);
        } else {
            roomTypesService.save($scope.roomType)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Typ pokoje byl úspěšně uložen");
                    $state.go('app.static.codetables');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };
};