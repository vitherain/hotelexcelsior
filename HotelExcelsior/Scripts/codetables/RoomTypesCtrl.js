﻿module.exports = function ($scope, $rootScope, roomTypesService, constants, flashService) {

    $scope.roomTypes = [];

    roomTypesService.readAll()
        .then(function(response) {
            $scope.roomTypes = response;
        })
        .catch(function () {
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst typy pokojů");
        });
};