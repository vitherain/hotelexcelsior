﻿module.exports = function() {

    function RoomTypeDTO(roomTypeArg) {
        this.id = roomTypeArg.Id ? roomTypeArg.Id : undefined;
        this.description = roomTypeArg.Description ? roomTypeArg.Description : undefined;
        this.doubleBedsCount = (roomTypeArg.DoubleBedsCount !== undefined && !isNaN(roomTypeArg.DoubleBedsCount)) ? roomTypeArg.DoubleBedsCount : undefined;
        this.singleBedsCount = (roomTypeArg.SingleBedsCount !== undefined && !isNaN(roomTypeArg.SingleBedsCount)) ? roomTypeArg.SingleBedsCount : undefined;
    }

    RoomTypeDTO.prototype.maxCapacity = function () {
        return (this.doubleBedsCount * 2) + this.singleBedsCount;
    };

    return RoomTypeDTO;
};