﻿module.exports = function (httpFacade, WayOfReservationDTO) {

    var _readAll = function () {
        return httpFacade.readAllWaysOfReservation().then(function (response) {
            return response.data.map(function (wOR) {
                return new WayOfReservationDTO(wOR);
            });
        });
    };

    var _readOne = function (wORId) {
        return httpFacade.readWayOfReservation(wORId).then(function (response) {
            return new WayOfReservationDTO(response.data);
        });
    };

    var _save = function (wayOfReservation) {
        if (wayOfReservation.id) {
            return httpFacade.updateWayOfReservation(wayOfReservation.id, wayOfReservation);
        } else {
            return httpFacade.createWayOfReservation(wayOfReservation);
        }
    };

    return {
        readAll: _readAll,
        readOne: _readOne,
        save: _save
    };
};