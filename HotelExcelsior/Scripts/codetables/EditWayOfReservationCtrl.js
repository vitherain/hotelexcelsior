﻿module.exports = function ($scope, $rootScope, $state, waysOfReservationService, constants, wayOfReservation, formUtilsService, flashService) {

    if (wayOfReservation === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst způsob rezervace");
    }

    $scope.wayOfReservation = wayOfReservation;
    $scope.isNew = wayOfReservation.id ? false : true;

    $scope.submitForm = function () {
        if ($scope.wayOfReservationForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.wayOfReservationForm);
        } else {
            waysOfReservationService.save($scope.wayOfReservation)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Způsob rezervace byl úspěšně uložen");
                    $state.go('app.static.codetables');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };
};