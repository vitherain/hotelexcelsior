﻿module.exports = function ($scope, $rootScope, waysOfReservationService, constants, flashService) {

    $scope.waysOfReservation = [];

    waysOfReservationService.readAll()
        .then(function (response) {
            $scope.waysOfReservation = response;
        })
        .catch(function () {
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst způsoby rezervace");
        });
};