module.exports = function(modalService) {

    var _setTouchedAndDirtyErrorFields = function(form) {
        angular.forEach(form.$error, function (field) {
            angular.forEach(field, function(errorField){
                errorField.$setTouched();
                errorField.$setDirty();
            })
        });
    };

    return {
        setTouchedAndDirtyErrorFields: _setTouchedAndDirtyErrorFields
    };
};