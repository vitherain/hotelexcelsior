module.exports = {

    error: 'error',
    currentUser: 'currentUser',
    selectedRole: 'selectedRole',
    datatableALengthMenuSmaller: [[5, 10, 25, -1], [5, 10, 25, 'Vše']],
    datatableALengthMenu: [10, 15, 25, 50, 100],
    allowedImageTypes: ['image/jpeg', 'image/gif', 'image/bmp', 'image/png']
};