module.exports = function ($http, appUtils) {

    var _authenticate = function (headers) {
        return $http.get(appUtils.basePath() + '/user', {headers : headers});
    };

    var _logout = function () {
        return $http.post(appUtils.basePath() + '/logout');
    };

    var _readAllRoomTypes = function () {
        return $http.get(appUtils.basePath() + '/api/RoomType');
    };

    var _readRoomType = function (roomTypeId) {
        return $http.get(appUtils.basePath() + '/api/RoomType/' + roomTypeId);
    };

    var _createRoomType = function (data) {
        return $http.post(appUtils.basePath() + '/api/RoomType', data);
    };

    var _updateRoomType = function (roomTypeId, data) {
        return $http.put(appUtils.basePath() + '/api/RoomType/' + roomTypeId, data);
    };

    var _readAllWaysOfReservation = function () {
        return $http.get(appUtils.basePath() + '/api/WayOfReservation');
    };

    var _readWayOfReservation = function (wORId) {
        return $http.get(appUtils.basePath() + '/api/WayOfReservation/' + wORId);
    };

    var _createWayOfReservation = function (data) {
        return $http.post(appUtils.basePath() + '/api/WayOfReservation', data);
    };

    var _updateWayOfReservation = function (wORId, data) {
        return $http.put(appUtils.basePath() + '/api/WayOfReservation/' + wORId, data);
    };

    var _readAllRooms = function () {
        return $http.get(appUtils.basePath() + '/api/Room');
    };

    var _readRoom = function (roomId) {
        return $http.get(appUtils.basePath() + '/api/Room/' + roomId);
    };

    var _createRoom = function (data) {
        return $http.post(appUtils.basePath() + '/api/Room', data);
    };

    var _updateRoom = function (roomId, data) {
        return $http.put(appUtils.basePath() + '/api/Room/' + roomId, data);
    };

    var _readAllGuests = function () {
        return $http.get(appUtils.basePath() + '/api/Guest');
    };

    var _readGuest = function (guestId) {
        return $http.get(appUtils.basePath() + '/api/Guest/' + guestId);
    };

    var _createGuest = function (data) {
        return $http.post(appUtils.basePath() + '/api/Guest', data);
    };

    var _updateGuest = function (guestId, data) {
        return $http.put(appUtils.basePath() + '/api/Guest/' + guestId, data);
    };

    var _readAllReservations = function () {
        return $http.get(appUtils.basePath() + '/api/Reservation');
    };

    var _readReservation = function (reservationId) {
        return $http.get(appUtils.basePath() + '/api/Reservation/' + reservationId);
    };

    var _createReservation = function (data) {
        return $http.post(appUtils.basePath() + '/api/Reservation', data);
    };

    var _updateReservation = function (reservationId, data) {
        return $http.put(appUtils.basePath() + '/api/Reservation/' + reservationId, data);
    };

    var _deleteReservation = function (reservationId) {
        return $http.delete(appUtils.basePath() + '/api/Reservation/' + reservationId);
    };

    var _readAllAccommodation = function () {
        return $http.get(appUtils.basePath() + '/api/Accommodation');
    };

    var _readAccommodation = function (accommodationId) {
        return $http.get(appUtils.basePath() + '/api/Accommodation/' + accommodationId);
    };

    var _createAccommodation = function (data) {
        return $http.post(appUtils.basePath() + '/api/Accommodation', data);
    };

    var _updateAccommodation = function (accommodationId, data) {
        return $http.put(appUtils.basePath() + '/api/Accommodation/' + accommodationId, data);
    };

    return {
        authenticate: _authenticate,
        logout: _logout,
        readAllRoomTypes: _readAllRoomTypes,
        readRoomType: _readRoomType,
        createRoomType: _createRoomType,
        updateRoomType: _updateRoomType,
        readAllWaysOfReservation: _readAllWaysOfReservation,
        readWayOfReservation: _readWayOfReservation,
        createWayOfReservation: _createWayOfReservation,
        updateWayOfReservation: _updateWayOfReservation,
        readAllRooms: _readAllRooms,
        readRoom: _readRoom,
        createRoom: _createRoom,
        updateRoom: _updateRoom,
        readAllGuests: _readAllGuests,
        readGuest: _readGuest,
        createGuest: _createGuest,
        updateGuest: _updateGuest,
        readAllReservations: _readAllReservations,
        readReservation: _readReservation,
        createReservation: _createReservation,
        updateReservation: _updateReservation,
        deleteReservation: _deleteReservation,
        readAllAccommodation: _readAllAccommodation,
        readAccommodation: _readAccommodation,
        createAccommodation: _createAccommodation,
        updateAccommodation: _updateAccommodation
    }
};