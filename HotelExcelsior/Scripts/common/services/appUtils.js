module.exports = function($location) {

    var _basePath = function() {
        var absUrl = $location.absUrl();
        var basePath = absUrl
            .replace(
                $location.protocol() + '://' +
                    $location.host() +
                    (($location.port() && $location.port() !== 80) ? ':' + $location.port() : ''),
                '')
            .replace('/#' + $location.url(), '');

        return basePath;
    };

    return {
        basePath: _basePath
    };
};