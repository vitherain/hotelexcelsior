module.exports = function($rootScope, $timeout, FlashMessage) {

    var queue = [];
    var currentMessage = "";

    var _setAndShowErrorMessageWithoutStateChange = function(message, duration) {
        _setErrorMessage(message, duration);
        show();
    };

    var _setErrorMessage = function(message, duration) {
        queue.push(new FlashMessage("danger", message, duration));
    };

    var _setAndShowErrorMessageFor10SecondsWithoutStateChange = function(message) {
        _setErrorMessageFor10Seconds(message);
        show();
    };

    var _setErrorMessageFor10Seconds = function(message) {
        queue.push(new FlashMessage("danger", message, 10000));
    };

    var _setAndShowSuccessMessageWithoutStateChange = function(message, duration) {
        _setSuccessMessage(message, duration);
        show();
    };

    var _setSuccessMessage = function(message, duration) {
        queue.push(new FlashMessage("success", message, duration));
    };

    var _setAndShowSuccessMessageFor10SecondsWithoutStateChange = function(message) {
        _setSuccessMessageFor10Seconds(message);
        show();
    };

    var _setSuccessMessageFor10Seconds = function(message) {
        queue.push(new FlashMessage("success", message, 10000));
    };

    var _setAndShowMessageWithoutStateChange = function(type, message, duration) {
        _setMessage(type, message, duration);
        show();
    };

    var _setMessage = function(type, message, duration) {
        queue.push(new FlashMessage(type, message, duration));
    };

    var _setAndShowMessageFor10SecondsWithoutStateChange = function(type, message) {
        _setMessageFor10Seconds(type, message);
        show();
    };

    var _setMessageFor10Seconds = function(type, message) {
        queue.push(new FlashMessage(type, message, 10000));
    };

    var _showMessageUnexpectedErrorFor10SecondsWithoutStateChange = function() {
        queue.push(new FlashMessage("danger", "Došlo k neočekávané chybě", 10000));
        show();
    };

    var _getMessage = function() {
        return currentMessage;
    };

    $rootScope.$on("$stateChangeSuccess", function() {
        show();
    });

    function show() {
        currentMessage = queue.shift() || "";
        $rootScope.showFlashAlert = true;

        if (currentMessage.duration) {
            $timeout(function() {
                $rootScope.showFlashAlert = false;
            }, currentMessage.duration);
        }
    }

    return {
        setAndShowErrorMessageWithoutStateChange: _setAndShowErrorMessageWithoutStateChange,
        setErrorMessage: _setErrorMessage,
        setAndShowErrorMessageFor10SecondsWithoutStateChange: _setAndShowErrorMessageFor10SecondsWithoutStateChange,
        setErrorMessageFor10Seconds: _setErrorMessageFor10Seconds,
        setAndShowSuccessMessageWithoutStateChange: _setAndShowSuccessMessageWithoutStateChange,
        setSuccessMessage: _setSuccessMessage,
        setAndShowSuccessMessageFor10SecondsWithoutStateChange: _setAndShowSuccessMessageFor10SecondsWithoutStateChange,
        setSuccessMessageFor10Seconds: _setSuccessMessageFor10Seconds,
        setAndShowMessageWithoutStateChange: _setAndShowMessageWithoutStateChange,
        setMessage: _setMessage,
        setAndShowMessageFor10SecondsWithoutStateChange: _setAndShowMessageFor10SecondsWithoutStateChange,
        setMessageFor10Seconds: _setMessageFor10Seconds,
        showMessageUnexpectedErrorFor10SecondsWithoutStateChange: _showMessageUnexpectedErrorFor10SecondsWithoutStateChange,
        getMessage: _getMessage
    };
};