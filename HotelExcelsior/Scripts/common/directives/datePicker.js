module.exports = function () {
    return {
        restrict: 'E',
        template:
                '<p class="input-group date-picker">' +
                    '<input type="hidden" id="{{ inputId }}" name="{{ name }}" ng-model="ngModel" uib-datepicker-popup ' +
                        'is-open="popup.opened" datepicker-options="dateOptions" ' +
                        'current-text="Dnes" clear-text="Vymazat" close-text="Zavřít">' +
                    '<span class="form-control default-pointer date-picker-visible-field" ng-bind="ngModel | date:\'dd.MM.yyyy\'"></span>' +
                    '<span class="input-group-btn">' +
                        '<button type="button" class="btn btn-info" ng-click="openPopup()">' +
                            '<span class="glyphicon glyphicon-calendar"></span>' +
                        '</button>' +
                    '</span>' +
                '</p>',
        replace: true,
        require: 'ngModel',
        scope: {
            ngModel: '=',
            inputId: '@input',
            name: '@name'
        },
        controller: function($scope) {
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.popup = {
                opened: false
            };

            $scope.openPopup = function() {
                $scope.popup.opened = true;
            };
        },
        link: function (scope, element, attrs, ctrl) {
            element.find('button').bind('click', function() {
                ctrl.$setTouched();
            });
        }
    };
};