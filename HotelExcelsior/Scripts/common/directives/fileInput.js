module.exports = function (constants) {
    return {
        restrict: 'E',
        template:
                '<div>' +
                    '<span name="{{ sizeFormName }}" ng-model="ngModel.size" ng-hide="true"></span>' +
                    '<span name="{{ typeFormName }}" ng-model="ngModel.type" ng-hide="true"></span>' +
                    '<div>' +
                        '<input id="{{ inputId }}" name="{{ name }}" type="file" class="btn btn-default btn-file" ng-model="ngModel" file-upload>' +
                        '<a class="btn btn-md btn-success" ng-click="clearFileInput()" ng-show="file">' +
                            '<span class="glyphicon glyphicon-trash"></span> Odebrat vybraný soubor' +
                        '</a>' +
                    '</div>' +
                '</div>',
        replace: true,
        require: 'ngModel',
        scope: {
            ngModel: '=',
            inputId: '@input',
            name: '@name',
            file: '=',
            sizeFormName: '@',
            typeFormName: '@'
        },
        controller: function($scope) {
            $scope.$watch('sizeFormName', function(newValue) {
                if (newValue && $scope.background) {
                    //size of file must be less than 1 Mb
                    newValue.$setValidity("size", $scope.background.size <= (1 * 1024 * 1024));
                }
            });

            function isOneOfAllowedImageTypes(backgroundType) {
                var allowedImageTypes = constants.allowedImageTypes;

                if (!backgroundType) {
                    return false;
                }

                return allowedImageTypes.indexOf(backgroundType.toLowerCase()) > -1;
            }

            $scope.$watch('typeFormName', function(newValue) {
                if (newValue && $scope.background) {
                    newValue.$setValidity("type", isOneOfAllowedImageTypes($scope.background.type));
                }
            });

            $scope.$on("fileSelected", function (event, args) {
                $scope.$apply(function () {
                    $scope.file = args.file;
                });
            });

            $scope.clearFileInput = function() {
                $scope.file = null;
                $scope.ngModel = null;
            };
        },
        link: function(scope, element, attrs, ctrl) {
            element.find('a').bind('click', function() {
                element.find('input').val('');
            });
        }
    };
};