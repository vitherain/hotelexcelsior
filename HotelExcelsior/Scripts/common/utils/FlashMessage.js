module.exports = function() {

    function FlashMessage(type, message, duration) {
        this.type = type;
        this.message = message;
        this.duration = duration;
    }

    return FlashMessage;
};