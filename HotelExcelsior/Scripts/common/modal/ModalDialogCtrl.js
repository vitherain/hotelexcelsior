module.exports = function($scope, $uibModalInstance, heading, message) {
    $scope.heading = heading;
    $scope.message = message;

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
};