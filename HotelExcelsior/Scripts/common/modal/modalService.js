module.exports = function($uibModal) {

    var _openModalDialog = function(heading, message) {

        var modalInstance = $uibModal.open({
            template: require('./modalDialog.html'),
            controller: require('./ModalDialogCtrl.js'),
            size: 'sm',
            resolve: {
                heading: function () {
                    return heading;
                },
                message: function () {
                    return message;
                }
            }
        });

        return modalInstance;
    };

    var _openAlert = function(heading, message) {

        var modalInstance = $uibModal.open({
            template: require('./alert.html'),
            controller: require('./AlertCtrl.js'),
            size: 'sm',
            resolve: {
                heading: function () {
                    return heading;
                },
                message: function () {
                    return message;
                }
            }
        });

        return modalInstance;
    };

    return {
        openModalDialog: _openModalDialog,
        openAlert: _openAlert
    };
};