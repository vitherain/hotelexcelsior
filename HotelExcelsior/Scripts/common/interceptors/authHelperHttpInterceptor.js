module.exports = function($injector, $rootScope, flashService) {
    var _responseError = function(rejection) {
        if (rejection.status === 401) {
            $injector.get('authService').logout().then(function() {
                $injector.get('$state').go("app.static.login");
            });
        }
    };

    return {
        responseError: _responseError
    };
};