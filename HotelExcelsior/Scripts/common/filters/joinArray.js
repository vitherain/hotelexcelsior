module.exports = function() {

    return function (array, separator) {
        if (separator) {
            return array.join(separator);
        }
        return array.join(', ');
    }
};