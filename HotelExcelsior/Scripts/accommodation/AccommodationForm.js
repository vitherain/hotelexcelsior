﻿module.exports = function (ReservationDTO) {

    function AccommodationForm(accommodationArg) {
        this.id = accommodationArg.Id ? accommodationArg.Id : undefined;
        this.checkInDate = accommodationArg.CheckInDate ? new Date(accommodationArg.CheckInDate) : undefined;
        this.checkOutDate = accommodationArg.CheckOutDate ? new Date(accommodationArg.CheckOutDate) : undefined;
        this.reservation = accommodationArg.Reservation ? new ReservationDTO(accommodationArg.Reservation) : undefined;
        this.reservationId = accommodationArg.Reservation ? accommodationArg.Reservation.Id : undefined;
    }

    return AccommodationForm;
};