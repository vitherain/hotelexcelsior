﻿module.exports = function (ReservationDTO) {

    function AccommodationDTO(accommodationArg) {
        this.id = accommodationArg.Id ? accommodationArg.Id : undefined;
        this.checkInDate = accommodationArg.CheckInDate ? new Date(accommodationArg.CheckInDate) : undefined;
        this.checkOutDate = accommodationArg.CheckOutDate ? new Date(accommodationArg.CheckOutDate) : undefined;
        this.reservation = accommodationArg.Reservation ? new ReservationDTO(accommodationArg.Reservation) : undefined;
    }

    AccommodationDTO.prototype.guestInfo = function () {
        return (this.reservation && this.reservation.guest) ? this.reservation.guest.fullName() + ', ' + this.reservation.guest.identityCardNumber : undefined;
    };

    AccommodationDTO.prototype.roomName = function () {
        return (this.reservation && this.reservation.room) ? this.reservation.room.name : undefined;
    };

    AccommodationDTO.prototype.isNotHistoric = function () {
        return !this.isHistoric();
    };

    AccommodationDTO.prototype.isHistoric = function () {
        if (this.checkOutDate) {
            return this.checkOutDate.getTime() < new Date().getTime();
        }

        return false;
    };

    AccommodationDTO.prototype.isNotClosed = function () {
        return !this.isClosed();
    };

    AccommodationDTO.prototype.isClosed = function () {
        return this.checkOutDate;
    };

    return AccommodationDTO;
};