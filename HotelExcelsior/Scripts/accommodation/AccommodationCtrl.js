﻿module.exports = function ($rootScope, $scope, accommodationService, flashService, modalService) {

    $scope.accommodation = [];

    function reloadData() {
        accommodationService.readAll()
        .then(function (response) {
            $scope.accommodation = response;
        })
        .catch(function () {
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst záznamy o ubytování");
        });
    }

    reloadData();

    $scope.closeAccommodation = function (singleAccommodation) {
        var modal = modalService.openModalDialog('Ukončit?', 'Skutečně si přejete ukončit ubytování?');

        modal.result.then(function () {
            accommodationService.closeAccommodation(singleAccommodation)
                .then(function () {
                    flashService.setAndShowSuccessMessageFor10SecondsWithoutStateChange('Ubytování bylo ukončeno.');
                    reloadData();
                })
                .catch(function () {
                    flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange('Nepodařilo se ukončit ubytování');
                });
        });
    };    
};