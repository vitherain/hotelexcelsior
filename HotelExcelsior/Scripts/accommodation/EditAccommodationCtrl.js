﻿module.exports = function ($scope, $rootScope, $state, accommodationService, constants, accommodation, reservations, formUtilsService, flashService) {

    if (accommodation === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst ubytování");
    }
    if (reservations === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst rezervace");
    }

    $scope.accommodation = accommodation;
    $scope.isNew = accommodation.id ? false : true;

    $scope.reservations = reservations;
    $scope.selectedReservation = {};

    $scope.selectReservation = function (reservationToSelect) {
        if (reservationToSelect) {
            $scope.accommodation.reservation = reservationToSelect;
            $scope.accommodation.reservationId = reservationToSelect.id;
            $scope.accommodation.checkInDate = reservationToSelect.dateIn;
            //clears ui-select - workaround
            angular.element('#reservationSelect a.btn').triggerHandler('click');
        }
    };

    $scope.submitForm = function() {
        if ($scope.accommodationForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.accommodationForm);
        } else {
            accommodationService.save($scope.accommodation)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Ubytování bylo úspěšně uloženo");
                    $state.go('app.static.accommodation');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };
};