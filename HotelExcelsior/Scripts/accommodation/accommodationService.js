﻿module.exports = function (httpFacade, AccommodationDTO, AccommodationForm) {

    var _readAll = function () {
        return httpFacade.readAllAccommodation().then(function (response) {
            return response.data.map(function (accommodation) {
                return new AccommodationDTO(accommodation);
            });
        });
    };

    var _readOne = function (accommodationId) {
        return httpFacade.readAccommodation(accommodationId).then(function (response) {
            return new AccommodationForm(response.data);
        });
    };

    var _save = function (accommodation) {
        if (accommodation.id) {
            return httpFacade.updateAccommodation(accommodation.id, accommodation);
        } else {
            return httpFacade.createAccommodation(accommodation);
        }
    };

    var _closeAccommodation = function (accommodation) {
        var checkOutDate = new Date();

        return _readOne(accommodation.id).then(function (readAccommodation) {
            readAccommodation.checkOutDate = checkOutDate;
            return _save(readAccommodation);
        }).then(function () {
            accommodation.checkOutDate = checkOutDate;
            return accommodation;
        });
    };

    return {
        readAll: _readAll,
        readOne: _readOne,
        save: _save,
        closeAccommodation: _closeAccommodation
    };
};