﻿module.exports = function ($scope, $rootScope, $state, roomsService, roomTypesService, constants, room, roomTypes, formUtilsService, flashService) {

    if (room === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst pokoj");
    }

    if (roomTypes === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst typy pokojů");
    }

    $scope.room = room;
    $scope.roomTypes = roomTypes;
    $scope.isNew = room.id ? false : true;

    $scope.submitForm = function () {
        if ($scope.roomForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.roomForm);
        } else {
            roomsService.save($scope.room)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Pokoj byl úspěšně uložen");
                    $state.go('app.static.rooms');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };
};