﻿module.exports = function(RoomTypeDTO) {

    function RoomDTO(roomArg) {
        this.id = roomArg.Id ? roomArg.Id : undefined;
        this.name = roomArg.Name ? roomArg.Name : undefined;
        this.smokingAllowed = ((roomArg.SmokingAllowed !== undefined) && (typeof roomArg.SmokingAllowed === "boolean")) ? roomArg.SmokingAllowed : undefined;
        this.roomType = roomArg.RoomType ? new RoomTypeDTO(roomArg.RoomType) : undefined;
    }

    return RoomDTO;
};