﻿module.exports = function() {

    function RoomForm(roomArg) {
        this.id = roomArg.Id ? roomArg.Id : undefined;
        this.name = roomArg.Name ? roomArg.Name : undefined;
        this.smokingAllowed = ((roomArg.SmokingAllowed !== undefined) && (typeof roomArg.SmokingAllowed === "boolean")) ? roomArg.SmokingAllowed : undefined;
        this.roomTypeId = roomArg.RoomType ? roomArg.RoomType.Id : undefined;
    }

    return RoomForm;
};