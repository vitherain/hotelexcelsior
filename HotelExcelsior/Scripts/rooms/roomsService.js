﻿module.exports = function (httpFacade, RoomDTO, RoomForm) {

    var _readAll = function () {
        return httpFacade.readAllRooms().then(function (response) {
            return response.data.map(function (room) {
                return new RoomDTO(room);
            });
        });
    };

    var _readOne = function (roomId) {
        return httpFacade.readRoom(roomId).then(function (response) {
            return new RoomForm(response.data);
        });
    };

    var _save = function (room) {
        if (room.id) {
            delete room.roomType;

            return httpFacade.updateRoom(room.id, room);
        } else {
            return httpFacade.createRoom(room);
        }
    };

    return {
        readAll: _readAll,
        readOne: _readOne,
        save: _save
    };
};