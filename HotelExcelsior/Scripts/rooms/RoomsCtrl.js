﻿module.exports = function ($scope, $rootScope, roomsService, constants, flashService) {

    $scope.rooms = [];

    roomsService.readAll()
        .then(function(response) {
            $scope.rooms = response;
        })
        .catch(function () {
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst pokoje");
        });
};