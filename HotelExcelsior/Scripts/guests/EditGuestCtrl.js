﻿module.exports = function ($scope, $rootScope, $state, guestsService, constants, guest, formUtilsService, flashService) {

    if (guest === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst hosta");
    }

    $scope.guest = guest;
    $scope.isNew = guest.id ? false : true;

    $scope.submitForm = function () {
        if ($scope.guestForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.guestForm);
        } else {
            guestsService.save($scope.guest)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Host byl úspěšně uložen");
                    $state.go('app.static.guests');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };
};