﻿module.exports = function() {

    function GuestDTO(guestArg) {
        this.id = guestArg.Id ? guestArg.Id : undefined;
        this.firstName = guestArg.FirstName ? guestArg.FirstName : undefined;
        this.lastName = guestArg.LastName ? guestArg.LastName : undefined;
        this.address = guestArg.Address ? guestArg.Address : undefined;
        this.identityCardNumber = guestArg.IdentityCardNumber ? guestArg.IdentityCardNumber : undefined;
    }

    GuestDTO.prototype.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    };

    return GuestDTO;
};