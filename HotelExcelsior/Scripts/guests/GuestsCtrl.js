﻿module.exports = function ($rootScope, $scope, guestsService, flashService) {

    $scope.guests = [];

    guestsService.readAll()
        .then(function (response) {
            $scope.guests = response;
        })
        .catch(function () {
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst hosty");
        });
};