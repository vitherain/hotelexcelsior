﻿module.exports = function (httpFacade, GuestDTO) {

    var _readAll = function () {
        return httpFacade.readAllGuests().then(function (response) {
            return response.data.map(function (guest) {
                return new GuestDTO(guest);
            });
        });
    };

    var _readOne = function (guestId) {
        return httpFacade.readGuest(guestId).then(function (response) {
            return new GuestDTO(response.data);
        });
    };

    var _save = function (guest) {
        if (guest.id) {
            return httpFacade.updateGuest(guest.id, guest);
        } else {
            return httpFacade.createGuest(guest);
        }
    };

    return {
        readAll: _readAll,
        readOne: _readOne,
        save: _save
    };
};