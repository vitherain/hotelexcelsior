﻿module.exports = function (httpFacade, ReservationDTO, ReservationForm) {

    var _readAll = function () {
        return httpFacade.readAllReservations().then(function (response) {
            return response.data.map(function (reservation) {
                return new ReservationDTO(reservation);
            });
        });
    };

    var _readAllWithoutAccommodationOnTheDay = function (date) {
        return _readAll().then(function (reservations) {
            var filteredReservations = [];

            angular.forEach(reservations, function (reservation) {
                if (!reservation.hasAccommodation && reservation.startsOn(date)) {
                    filteredReservations.push(reservation);
                }
            });

            return filteredReservations;
        });
    };

    var _readOne = function (reservationId) {
        return httpFacade.readReservation(reservationId).then(function (response) {
            return new ReservationForm(response.data);
        });
    };

    var _save = function (reservation) {
        if (reservation.id) {
            return httpFacade.updateReservation(reservation.id, reservation);
        } else {
            return httpFacade.createReservation(reservation);
        }
    };

    var _deleteOne = function (reservationId) {
        return httpFacade.deleteReservation(reservationId);
    };

    return {
        readAll: _readAll,
        readAllWithoutAccommodationOnTheDay: _readAllWithoutAccommodationOnTheDay,
        readOne: _readOne,
        save: _save,
        deleteOne: _deleteOne
    };
};