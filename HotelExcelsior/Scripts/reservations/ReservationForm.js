﻿module.exports = function () {

    function ReservationForm(reservationArg) {
        this.id = reservationArg.Id ? reservationArg.Id : undefined;
        this.dateIn = reservationArg.DateIn ? new Date(reservationArg.DateIn) : undefined;
        this.dateOut = reservationArg.DateOut ? new Date(reservationArg.DateOut) : undefined;
        this.wayOfReservationId = reservationArg.WayOfReservation ? reservationArg.WayOfReservation.Id : undefined;
        this.guestId = reservationArg.Guest ? reservationArg.Guest.Id : undefined;
        this.roomId = reservationArg.Room ? reservationArg.Room.Id : undefined;
        this.hasAccommodation = reservationArg.HasAccommodation ? reservationArg.HasAccommodation : false;
    }

    return ReservationForm;
};