﻿module.exports = function ($rootScope, $scope, reservationsService, flashService, modalService) {

    $scope.reservations = [];

    function reloadData() {
        reservationsService.readAll()
        .then(function (response) {
            $scope.reservations = response;
        })
        .catch(function () {
            flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst rezervace");
        });
    }
    
    reloadData();

    $scope.deleteReservation = function (reservation) {
        var modal = modalService.openModalDialog('Zrušit?', 'Skutečně si přejete zrušit tuto rezervaci?');

        modal.result.then(function () {
            reservationsService.deleteOne(reservation.id).then(function () {
                flashService.setAndShowSuccessMessageFor10SecondsWithoutStateChange('Rezervace byla zrušena');
                reloadData();
            })
            .catch(function () {
                flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange('Nepodařilo se zrušit rezervaci');
            });
        });
    };
};