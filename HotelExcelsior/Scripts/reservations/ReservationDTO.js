﻿module.exports = function (WayOfReservationDTO, GuestDTO, RoomDTO) {

    function ReservationDTO(reservationArg) {
        this.id = reservationArg.Id ? reservationArg.Id : undefined;
        this.dateIn = reservationArg.DateIn ? new Date(reservationArg.DateIn) : undefined;
        this.dateOut = reservationArg.DateOut ? new Date(reservationArg.DateOut) : undefined;
        this.wayOfReservation = reservationArg.WayOfReservation ? new WayOfReservationDTO(reservationArg.WayOfReservation) : undefined;
        this.guest = reservationArg.Guest ? new GuestDTO(reservationArg.Guest) : undefined;
        this.room = reservationArg.Room ? new RoomDTO(reservationArg.Room) : undefined;
        this.hasAccommodation = reservationArg.HasAccommodation ? reservationArg.HasAccommodation : false;
    }

    ReservationDTO.prototype.guestInfo = function () {
        return this.guest ? this.guest.fullName() + ', ' + this.guest.identityCardNumber : undefined;
    };

    ReservationDTO.prototype.roomName = function () {
        return this.room ? this.room.name : undefined;
    };

    ReservationDTO.prototype.wayOfReservationName = function () {
        return this.wayOfReservation ? this.wayOfReservation.name : undefined;
    };

    ReservationDTO.prototype.isHistoric = function () {
        if (this.dateOut) {
            var dateOutCopy = new Date(this.dateOut);
            dateOutCopy.setHours(0, 0, 0, 0);

            var today = new Date();
            today.setHours(0, 0, 0, 0);

            return dateOutCopy.getTime() < today.getTime();
        }

        return false;
    };

    ReservationDTO.prototype.startsToday = function () {
        if (this.dateIn) {
            var dateInCopy = new Date(this.dateIn);
            dateInCopy.setHours(0, 0, 0, 0);

            var today = new Date();
            today.setHours(0, 0, 0, 0);

            return dateInCopy.getTime() === today.getTime();
        }

        return true;
    };

    ReservationDTO.prototype.startsOn = function (date) {
        if (this.dateIn) {
            var dateInCopy = new Date(this.dateIn);
            dateInCopy.setHours(0, 0, 0, 0);

            date.setHours(0, 0, 0, 0);

            return dateInCopy.getTime() === date.getTime();
        }

        return true;
    };

    ReservationDTO.prototype.isNotHistoric = function () {
        return !this.isHistoric();
    };

    return ReservationDTO;
};