﻿module.exports = function ($scope, $rootScope, $state, reservationsService, constants, reservation, guests, rooms, waysOfReservation, formUtilsService, flashService) {

    if (reservation === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst rezervaci");
    }
    if (guests === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst hosty");
    }
    if (rooms === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst pokoje");
    }
    if (waysOfReservation === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst způsoby rezervace");
    }

    $scope.reservation = reservation;
    $scope.isNew = reservation.id ? false : true;

    $scope.guests = guests;
    $scope.rooms = rooms;
    $scope.waysOfReservation = waysOfReservation;

    $scope.submitForm = function () {
        if ($scope.reservationForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.reservationForm);
        } else {
            reservationsService.save($scope.reservation)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Rezervace byla úspěšně uložena");
                    $state.go('app.static.reservations');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };
};