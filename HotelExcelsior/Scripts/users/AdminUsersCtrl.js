module.exports = function ($scope, $filter, $rootScope, modalService, flashService, constants, userService, users) {

    if (users === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst uživatele");
    }

    $scope.users = users;
    $scope.smartTableLengthMenu = constants.datatableALengthMenu;
    $scope.itemsByPage = 15;

    function reloadData() {
        userService.readAllUsers().then(function(users) {
            $scope.users = users;
        });
    }

    $scope.enableUser = function (user) {
        if (!user.enabled) {
            var modal = modalService.openModalDialog('Aktivovat?', 'Opravdu si přejete tomuto uživateli aktivovat účet?');

            modal.result.then(function() {
                userService.enableUser(user)
                    .then(function() {
                        flashService.setAndShowSuccessMessageFor10SecondsWithoutStateChange("Účet uživatele byl úspěšně aktivován");
                        reloadData();
                    })
                    .catch(function() {
                        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se aktivovat účet uživatele");
                    });
            });
        }
    };

    $scope.disableUser = function (user) {
        if (user.enabled) {
            var modal = modalService.openModalDialog('Deaktivovat?', 'Opravdu si přejete tomuto uživateli deaktivovat účet? Přijde pak o možnost přihlásit se!');

            modal.result.then(function() {
                userService.disableUser(user)
                    .then(function() {
                        flashService.setAndShowSuccessMessageFor10SecondsWithoutStateChange("Účet uživatele byl úspěšně deaktivován");
                        reloadData();
                    })
                    .catch(function() {
                        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se deaktivovat účet uživatele");
                    });
            });
        }
    };
};