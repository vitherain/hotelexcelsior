module.exports = function ($scope, $rootScope, modalService, user, userService, $state,
                           companyService, constants, formUtilsService, flashService) {

    if (user === constants.error) {
        flashService.setAndShowErrorMessageFor10SecondsWithoutStateChange("Nepodařilo se načíst uživatele");
    }

    $scope.usr = user;

    $scope.isNew = user.id ? false : true;

    $scope.showAddRoleSelect = false;

    companyService.readAllCompanies().then(function(companies) {
        $scope.companies = companies;
    });

    userService.readAllRoles().then(function(roles) {
        $scope.roles = roles;
    });

    $scope.addRole = function() {
        if ($scope.roleToAdd) {
            $scope.usr.roles.push($scope.roleToAdd);
            $scope.roleToAdd = '';
        }
    };

    $scope.removeRole = function (role) {
        $scope.userForm.rolesCount.$setTouched();

        var index = $scope.usr.roles.indexOf(role);
        $scope.usr.roles.splice(index, 1);
    };

    $scope.isRoleNewForUserFilter = function() {
        return function (role) {
            return $scope.usr.roles.indexOf(role) < 0;
        }
    };

    $scope.userHasAllRoles = function() {
        return $scope.usr.roles && $scope.roles && $scope.usr.roles.length === $scope.roles.length;
    };

    $scope.submitForm = function() {
        if ($scope.userForm.$invalid) {
            formUtilsService.setTouchedAndDirtyErrorFields($scope.userForm);
        } else {
            userService.saveUser($scope.usr)
                .then(function (response) {
                    flashService.setSuccessMessageFor10Seconds("Uživatel byl úspěšně uložen");
                    $state.go('app.admin.users');
                }).catch(function () {
                    flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
                });
        }
    };

    $scope.$watch('userForm.rolesCount', function (newValue) {
        if (newValue) {
            newValue.$setValidity("count", $scope.usr.roles.length > 0);
        }
    });

    $scope.$watch('usr.roles.length', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            $scope.userForm.rolesCount.$dirty = true;
            $scope.userForm.$setDirty();
            $scope.userForm.rolesCount.$setValidity("count", newValue > 0);
        }
    });
};