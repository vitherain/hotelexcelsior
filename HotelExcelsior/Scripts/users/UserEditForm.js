module.exports = function() {

    function UserEditForm(userArg) {
        this.id = userArg.id ? userArg.id : undefined;
        this.enabled = userArg.enabled ? userArg.enabled : false;
        this.firstName = userArg.firstName ? userArg.firstName : undefined;
        this.lastName = userArg.lastName ? userArg.lastName : undefined;
        this.companyId = userArg.companyId ? userArg.companyId : undefined;
        this.email = userArg.email ? userArg.email : undefined;
        this.telephone = userArg.telephone ? userArg.telephone : undefined;
        this.address = userArg.address ? userArg.address : undefined;
        this.roles = (userArg.roles && userArg.roles.length) ? userArg.roles : [];
    }

    UserEditForm.prototype.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    };

    return UserEditForm;
};