module.exports = function(httpFacade, UserDTO, UserEditForm, UserForComboBoxDTO) {

    var _readUser = function(userId) {
        return httpFacade.readUser(userId).then(function(response) {
            return new UserEditForm(response.data);
        });
    };

    var _readAllUsers = function() {
        return httpFacade.readAllUsers().then(function(response) {
            return response.data.map(function(user) {
                return new UserDTO(user);
            });
        });
    };

    var _saveUser = function(user) {
        if (user.id) {
            return httpFacade.updateUser(user.id, user);
        } else {
            return httpFacade.createUser(user);
        }
    };

    var _enableUser = function(user) {
        return _readUser(user.id).then(function(readUser) {
            readUser.enabled = true;
            return _saveUser(readUser);
        }).then(function() {
            user.enabled = true;
            return user;
        });
    };

    var _disableUser = function(user) {
        return _readUser(user.id).then(function(readUser) {
            readUser.enabled = false;
            return _saveUser(readUser);
        }).then(function() {
            user.enabled = false;
            return user;
        });
    };

    var _readHumanResourcesWorkersForCompany = function(companyId) {
        return httpFacade.readHumanResourcesWorkersForCompany(companyId).then(function(response) {
            return response.data.map(function(hRWorker) {
                return new UserForComboBoxDTO(hRWorker);
            });
        });
    };

    var _readAllRoles = function() {
        return httpFacade.readAllRoles().then(function(response) {
            return response.data;
        });
    };

    return {
        readUser: _readUser,
        readAllUsers: _readAllUsers,
        saveUser: _saveUser,
        enableUser: _enableUser,
        disableUser: _disableUser,
        readHumanResourcesWorkersForCompany: _readHumanResourcesWorkersForCompany,
        readAllRoles: _readAllRoles
    }
};