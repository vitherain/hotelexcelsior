module.exports = function() {

    function UserForComboBoxDTO(userArg) {
        this.id = userArg.id ? userArg.id : undefined;
        this.firstName = userArg.firstName ? userArg.firstName : undefined;
        this.lastName = userArg.lastName ? userArg.lastName : undefined;
    }

    UserForComboBoxDTO.prototype.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    };

    return UserForComboBoxDTO;
};