module.exports = function(CompanyDTO) {

    function UserDTO(userArg) {
        this.id = userArg.id ? userArg.id : undefined;
        this.photoUrl = userArg.photoUrl ? userArg.photoUrl : undefined;
        this.firstName = userArg.firstName ? userArg.firstName : undefined;
        this.lastName = userArg.lastName ? userArg.lastName : undefined;
        this.roles = (userArg.roles && userArg.roles.length) ? userArg.roles : [];
        this.email = userArg.email ? userArg.email : undefined;
        this.enabled = userArg.enabled ? userArg.enabled : false;
        this.verified = userArg.verified ? userArg.verified : false;
        this.company = userArg.company ? new CompanyDTO(userArg.company) : undefined;
    }

    UserDTO.prototype.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    };

    return UserDTO;
};