module.exports = function ($window, $scope, $rootScope, $location, authService, flashService, $state) {

    $scope.logout = function() {
        authService.logout().catch(function () {
            flashService.showMessageUnexpectedErrorFor10SecondsWithoutStateChange();
        });
    };

    $scope.backInHistory = function() {
        $window.history.back();
    };
};