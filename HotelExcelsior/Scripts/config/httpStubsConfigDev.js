var fakeApi = angular.module('fakeApi', ['ngMockE2E']);
fakeApi
    .config(function($provide) {
        $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
    })
    .run(function($httpBackend) {
        var mockCompanies = [
            {
                "id": 1,
                "name": "Inventi",
                "tin": "12345678",
                "contactPerson": "Josef Smrk",
                "phone": "608608608",
                "email": "pepa@inventi.cz"
            },
            {
                "id": 2,
                "name": "Vodafone",
                "tin": "87654321",
                "contactPerson": "Thomas Brauner",
                "phone": "777666555",
                "email": "thomas@vodafone.cz"
            }
        ];

        var mockUsers = [
            {
                "id":78,
                "photoUrl": "http://s30.postimg.org/ogtgcwg3x/image.jpg",
                "email":"test.testovic@inventi.cz",
                "firstName":"Test",
                "lastName":"Testovič",
                "companyId": 1,
                "company": mockCompanies[0],
                "telephone":"+420654987321",
                "address": "Balounova 1, Nové město nad Metují, 15689",
                "roles":["RESOURCE_MANAGER","ADMIN","TRAINEE"],
                "enabled":true,
                "name":"Test Testovič",
                "authorities":[
                    {"authority":"ROLE_RESOURCE_MANAGER"},
                    {"authority":"ROLE_ADMIN"},
                    {"authority":"ROLE_TRAINEE"}
                ],
                "accountNonExpired":true,
                "accountNonLocked":true,
                "credentialsNonExpired":true
            },
            {
                "id":1,
                "photoUrl": "http://s30.postimg.org/j7yff0xot/image.jpg",
                "email":"jan.pivous@inventi.cz",
                "firstName":"Jan",
                "lastName":"Pivous",
                "companyId": 1,
                "company": mockCompanies[0],
                "telephone":"+420721000000",
                "address": "Slívová 518/20, 140 00 Praha 10",
                "roles":["RESOURCE_MANAGER", "ADMIN"],
                "enabled":true,
                "name":"Jan Pivous",
                "authorities":[
                    {"authority":"ROLE_RESOURCE_MANAGER"},
                    {"authority":"ROLE_ADMIN"}
                ],
                "accountNonExpired":true,
                "accountNonLocked":true,
                "credentialsNonExpired":true
            },
            {
                "id":2,
                "photoUrl": "http://s30.postimg.org/e80z72s25/image.jpg",
                "email":"alzbeta.mala@inventi.cz",
                "firstName":"Alžběta",
                "lastName":"Malá",
                "companyId": 2,
                "company": mockCompanies[1],
                "telephone":"+420603501508",
                "address": "Nerudova 5, 110 00 Praha 1",
                "roles":["TRAINER"],
                "enabled":true,
                "name":"Alžběta Malá",
                "authorities":[
                    {"authority":"ROLE_TRAINER"}
                ],
                "accountNonExpired":true,
                "accountNonLocked":true,
                "credentialsNonExpired":true
            },
            {
                "id":3,
                "photoUrl": "http://s30.postimg.org/e80z72s25/image.jpg",
                "email":"pavel.jedlicka@inventi.cz",
                "firstName":"Pavel",
                "lastName":"Jedlička",
                "companyId": 2,
                "company": mockCompanies[1],
                "telephone":"+420456789123",
                "address": "Kyperská 56, 156 89 Nová Paka",
                "roles":["RESOURCE_MANAGER"],
                "enabled":false,
                "name":"Pavel Jedlička",
                "authorities":[
                    {"authority":"ROLE_RESOURCE_MANAGER"}
                ],
                "accountNonExpired":true,
                "accountNonLocked":true,
                "credentialsNonExpired":true
            }
        ];

        var mockAcademies = [
            {
                "id": 1,
                "active": true,
                "startDate": new Date(2016, 0, 1),
                "endDate": new Date(2016, 11, 31),
                "name": "Java akademie",
                "backgroundUrl": "https://upload.wikimedia.org/wikipedia/commons/a/a4/Anatomy_of_a_Sunset-2.jpg",
                "focus": "Programování",
                "description": "Akademie týkající se Javy",
                "companyId": 1,
                "companyName": "Inventi",
                "guarantorId": 1,
                "lessonsCount": 6,
                "participants": [78, 1]
            },
            {
                "id": 2,
                "active": false,
                "startDate": new Date(2016, 0, 15),
                "endDate": new Date(2016, 5, 30),
                "name": "Testing akademie",
                "backgroundUrl": "http://s30.postimg.org/ogtgcwg3x/image.jpg",
                "focus": "Testování",
                "description": "Akademie týkající se testingu",
                "companyId": 2,
                "companyName": "Vodafone",
                "guarantorId": 3,
                "lessonsCount": 9,
                "participants": [1, 2]
            },
            {
                "id": 3,
                "active": true,
                "startDate": new Date(2016, 0, 8),
                "endDate": new Date(2016, 10, 30),
                "name": "Analytická akademie",
                "backgroundUrl": "http://s30.postimg.org/ogtgcwg3x/image.jpg",
                "focus": "Analýza",
                "description": "Akademie týkající se analýzy IS",
                "companyId": 1,
                "companyName": "Inventi",
                "guarantorId": 1,
                "lessonsCount": 5,
                "participants": [1, 2]
            },
            {
                "id": 4,
                "active": true,
                "startDate": new Date(2016, 2, 1),
                "endDate": new Date(2016, 11, 25),
                "name": ".NET akademie",
                "backgroundUrl": "http://s30.postimg.org/ogtgcwg3x/image.jpg",
                "focus": "Programování",
                "description": "Akademie týkající se .NET vývoje",
                "companyId": 2,
                "companyName": "Vodafone",
                "guarantorId": 3,
                "lessonsCount": 7,
                "participants": [78, 1]
            }
        ];

        var mockLessons1 = [
            {
                "id": 1,
                "date": new Date(2016, 0, 1),
                "name": "Základní pojmy Javy",
                "description": "Na této lekci budou probírány základní pojmy z programovacího jazyka Java",
                "trainerId": 1,
                "trainerName": "Jan Milan"
            },
            {
                "id": 2,
                "date": new Date(2016, 1, 28),
                "name": "Spring framework",
                "description": "Bude probrán Spring framework",
                "trainerId": 2,
                "trainerName": "Aleš Jarouš"
            }
        ];

        var mockLessons2 = [
            {
                "id": 3,
                "date": new Date(2015, 0, 15),
                "name": "Základní pojmy metodiky ISTQB",
                "description": "Základní pojmy metodiky ISTQB",
                "trainerId": 3,
                "trainerName": "Zdeněk Pán"
            },
            {
                "id": 4,
                "date": new Date(2015, 5, 21),
                "name": "Selenium testování",
                "description": "Znáte nástroj Selenium? Po této lekci jej budete znát dokonale!",
                "trainerId": 4,
                "trainerName": "Václav Levý"
            },
            {
                "id": 5,
                "date": new Date(2015, 9, 10),
                "name": "White box / black box testing",
                "description": "Lekce se zabývá procvičením těchto 2 druhů testů!",
                "trainerId": 4,
                "trainerName": "Václav Levý"
            }
        ];

        var mockLessons3 = [
            {
                "id": 3,
                "date": new Date(2015, 0, 15),
                "name": "UML",
                "description": "Základní pojmy UML",
                "trainerId": 3,
                "trainerName": "Zdeněk Pán"
            },
            {
                "id": 4,
                "date": new Date(2015, 5, 21),
                "name": "Class diagram",
                "description": "Zásadní UML diagram!",
                "trainerId": 4,
                "trainerName": "Václav Levý"
            },
            {
                "id": 5,
                "date": new Date(2015, 9, 10),
                "name": "Use case diagram",
                "description": "Zásadní diagram!",
                "trainerId": 4,
                "trainerName": "Václav Levý"
            }
        ];

        var mockLessons4 = [
            {
                "id": 3,
                "date": new Date(2015, 0, 15),
                "name": "C#",
                "description": "Základní pojmy C#",
                "trainerId": 3,
                "trainerName": "Zdeněk Pán"
            },
            {
                "id": 4,
                "date": new Date(2015, 5, 21),
                "name": "F#",
                "description": "Jeden z .NET jazyků!",
                "trainerId": 4,
                "trainerName": "Václav Levý"
            },
            {
                "id": 5,
                "date": new Date(2015, 9, 10),
                "name": "Entity framework",
                "description": "Oficiální ORM framework od Microsoftu!",
                "trainerId": 4,
                "trainerName": "Václav Levý"
            }
        ];

        var mockTrainers1 = [
            {
                "id": 1,
                "firstName": "Jan",
                "lastName":  "Milan"
            },
            {
                "id": 2,
                "firstName": "Aleš",
                "lastName":  "Jarouš"
            }
        ];

        var mockTrainers2 = [
            {
                "id": 3,
                "firstName": "Zdeněk",
                "lastName": "Pán"
            },
            {
                "id": 4,
                "firstName": "Václav",
                "lastName": "Levý"
            }
        ];

        var mockRoles = [
            "UNVERIFIED",
            "TRAINEE",
            "RESOURCE_MANAGER",
            "ADMIN"
        ];

        $httpBackend.whenGET('/user').respond(mockUsers[0]);
        $httpBackend.whenPOST('/logout').respond();

        $httpBackend.whenGET('/api/users/78').respond(mockUsers[0]);
        $httpBackend.whenGET('/api/users/1').respond(mockUsers[1]);
        $httpBackend.whenGET('/api/users/2').respond(mockUsers[2]);
        $httpBackend.whenGET('/api/users/3').respond(mockUsers[3]);
        $httpBackend.whenGET('/api/users').respond(mockUsers);

        $httpBackend.whenGET('/api/academies').respond(mockAcademies);
        $httpBackend.whenGET('/api/academies/1').respond(mockAcademies[0]);
        $httpBackend.whenGET('/api/academies/2').respond(mockAcademies[1]);
        $httpBackend.whenGET('/api/academies/3').respond(mockAcademies[2]);
        $httpBackend.whenGET('/api/academies/4').respond(mockAcademies[3]);
        $httpBackend.whenPOST('/api/academies').respond(function(method,url,data) {
            mockAcademies.push(JSON.parse(data));
            return [201, mockAcademies, {}];
        });
        $httpBackend.whenPUT('/api/academies/1').respond(function(method,url,data) {
            mockAcademies[0] = JSON.parse(data);
            return [200, mockAcademies[0], {}];
        });
        $httpBackend.whenPUT('/api/academies/2').respond(function(method,url,data) {
            mockAcademies[1] = JSON.parse(data);
            return [200, mockAcademies[1], {}];
        });
        $httpBackend.whenPUT('/api/academies/3').respond(function(method,url,data) {
            mockAcademies[2] = JSON.parse(data);
            return [200, mockAcademies[2], {}];
        });
        $httpBackend.whenPUT('/api/academies/4').respond(function(method,url,data) {
            mockAcademies[3] = JSON.parse(data);
            return [200, mockAcademies[3], {}];
        });

        $httpBackend.whenGET('/api/academies/1/participants').respond([mockUsers[0], mockUsers[1]]);
        $httpBackend.whenGET('/api/academies/2/participants').respond([mockUsers[1], mockUsers[2]]);
        $httpBackend.whenGET('/api/academies/3/participants').respond([mockUsers[1], mockUsers[2]]);
        $httpBackend.whenGET('/api/academies/4/participants').respond([mockUsers[0], mockUsers[1]]);

        $httpBackend.whenGET('/api/companies/1/trainers').respond(mockTrainers1);
        $httpBackend.whenGET('/api/companies/2/trainers').respond(mockTrainers2);
        $httpBackend.whenGET('/api/companies/1/humanResourcesWorkers').respond(mockTrainers1);
        $httpBackend.whenGET('/api/companies/2/humanResourcesWorkers').respond(mockTrainers2);

        $httpBackend.whenGET('/api/academies/1/lessons').respond(mockLessons1);
        $httpBackend.whenGET('/api/academies/2/lessons').respond(mockLessons2);
        $httpBackend.whenGET('/api/academies/3/lessons').respond(mockLessons3);
        $httpBackend.whenGET('/api/academies/4/lessons').respond(mockLessons4);

        $httpBackend.whenGET('/api/companies').respond(mockCompanies);

        $httpBackend.whenGET('/api/roles').respond(mockRoles);
        $httpBackend.whenPOST('/api/users').respond(function(method,url,data) {
            mockUsers.push(JSON.parse(data));
            return [200, mockUsers, {}];
        });
        $httpBackend.whenPUT('/api/users/78').respond(function(method,url,data) {
            mockUsers[0] = JSON.parse(data);
            return [200, mockUsers[0], {}];
        });
        $httpBackend.whenPUT('/api/users/1').respond(function(method,url,data) {
            mockUsers[1] = JSON.parse(data);
            return [200, mockUsers[1], {}];
        });
        $httpBackend.whenPUT('/api/users/2').respond(function(method,url,data) {
            mockUsers[2] = JSON.parse(data);
            return [200, mockUsers[2], {}];
        });

        $httpBackend.whenGET('/api/companies').respond(mockCompanies);
        $httpBackend.whenRoute('GET', '/api/companies/:id')
            .respond(function(method, url, data, headers, params) {
                return [200, mockCompanies[Number(params.id - 1)]];
            });
        $httpBackend.whenRoute('PUT', '/api/companies/:id')
            .respond(function(method, url, data, headers, params) {
                mockCompanies[Number(params.id - 1)] = JSON.parse(data);
                return [200, mockCompanies[Number(params.id - 1)]];
            });
        $httpBackend.whenPOST('/api/companies')
            .respond(function(method,url,data) {
                mockCompanies.push(JSON.parse(data));
                mockCompanies[mockCompanies.length - 1].id = mockCompanies.length;
                return [201, mockCompanies, {}];
        });
    });