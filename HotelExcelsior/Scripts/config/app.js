angular.module('hotel-excelsior', ['ngLodash', 'ui.select', 'ngSanitize', 'ngAnimate', 'angularFileUpload', 'ui.bootstrap', 'ui.router', 'ui.navbar', 'uiGmapgoogle-maps', 'ngMessages', 'LocalStorageModule', 'smart-table', 'fakeApi'])
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise("/static/home");

        $stateProvider.state('app', {
            abstract: true,
            url: '',
            controller: require('./../application/AppCtrl.js'),
            template: require('./../application/app.html')
        });

        $stateProvider.state('app.static', {
            abstract: true,
            url: '/static',
            controller: require('./../navbar/NavbarCtrl.js'),
            template: require('../navbar/app.static.nav.html')
        });

        $stateProvider.state('app.static.home', {
            url: '/home',
            template: require('../application/app.static.home.html')
        });

        $stateProvider.state('app.static.codetables', {
            url: '/codetables',
            views: {
                '': {
                    template: require('../codetables/app.static.codetables.html')
                },
                'waysOfReservation@app.static.codetables': {
                    controller: require('./../codetables/WaysOfReservationCtrl.js'),
                    template: require('../codetables/app.static.codetables.waysOfReservation.html')
                },
                'roomTypes@app.static.codetables': {
                    controller: require('./../codetables/RoomTypesCtrl.js'),
                    template: require('../codetables/app.static.codetables.roomTypes.html')
                }
            }
        });
        $stateProvider.state('app.static.rooms', {
            url: '/rooms',
            controller: require('./../rooms/RoomsCtrl.js'),
            template: require('../rooms/app.static.codetables.rooms.html')
        });
        $stateProvider.state('app.static.createWayOfReservation', {
            url: 'waysOfReservation/add',
            controller: require('./../codetables/EditWayOfReservationCtrl.js'),
            template: require('../codetables/app.static.codetables.editWayOfReservation.html'),
            resolve: {
                wayOfReservation: function() {
                    return {};
                }
            }
        });
        $stateProvider.state('app.static.editWayOfReservation', {
            url: 'waysOfReservation/edit/:wORId',
            controller: require('./../codetables/EditWayOfReservationCtrl.js'),
            template: require('../codetables/app.static.codetables.editWayOfReservation.html'),
            resolve: {
                wayOfReservation: function (waysOfReservationService, constants, $stateParams) {
                    return waysOfReservationService.readOne($stateParams.wORId).catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.createRoomType', {
            url: 'roomTypes/add',
            controller: require('./../codetables/EditRoomTypeCtrl.js'),
            template: require('../codetables/app.static.codetables.editRoomType.html'),
            resolve: {
                roomType: function () {
                    return {};
                }
            }
        });
        $stateProvider.state('app.static.editRoomType', {
            url: 'roomTypes/edit/:roomTypeId',
            controller: require('./../codetables/EditRoomTypeCtrl.js'),
            template: require('../codetables/app.static.codetables.editRoomType.html'),
            resolve: {
                roomType: function (roomTypesService, constants, $stateParams) {
                    return roomTypesService.readOne($stateParams.roomTypeId).catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.createRoom', {
            url: 'rooms/add',
            controller: require('./../rooms/EditRoomCtrl.js'),
            template: require('../rooms/app.static.codetables.editRoom.html'),
            resolve: {
                room: function (RoomForm) {
                    return new RoomForm(
                        {
                            SmokingAllowed: false
                        }
                    );
                },
                roomTypes: function (roomTypesService, constants) {
                    return roomTypesService.readAll().catch(function () {
                        return constants.error;
                    });
                }
            }
        });
        $stateProvider.state('app.static.editRoom', {
            url: 'rooms/edit/:roomId',
            controller: require('./../rooms/EditRoomCtrl.js'),
            template: require('../rooms/app.static.codetables.editRoom.html'),
            resolve: {
                room: function (roomsService, constants, $stateParams) {
                    return roomsService.readOne($stateParams.roomId).catch(function () {
                        return constants.error;
                    });
                },
                roomTypes: function (roomTypesService, constants) {
                    return roomTypesService.readAll().catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.guests', {
            url: '/guests',
            controller: require('./../guests/GuestsCtrl.js'),
            template: require('../guests/app.static.guests.html')
        });

        $stateProvider.state('app.static.createGuest', {
            url: 'guests/add',
            controller: require('./../guests/EditGuestCtrl.js'),
            template: require('../guests/app.static.editGuest.html'),
            resolve: {
                guest: function () {
                    return {};
                }
            }
        });
        $stateProvider.state('app.static.editGuest', {
            url: 'guests/edit/:guestId',
            controller: require('./../guests/EditGuestCtrl.js'),
            template: require('../guests/app.static.editGuest.html'),
            resolve: {
                guest: function (guestsService, constants, $stateParams) {
                    return guestsService.readOne($stateParams.guestId).catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.reservations', {
            url: '/reservations',
            controller: require('./../reservations/ReservationsCtrl.js'),
            template: require('../reservations/app.static.reservations.html')
        });

        $stateProvider.state('app.static.createReservation', {
            url: 'reservations/add',
            controller: require('./../reservations/EditReservationCtrl.js'),
            template: require('../reservations/app.static.editReservation.html'),
            resolve: {
                reservation: function () {
                    return {};
                },
                guests: function (guestsService, constants) {
                    return guestsService.readAll().catch(function () {
                        return constants.error;
                    });
                },
                rooms: function (roomsService, constants) {
                    return roomsService.readAll().catch(function () {
                        return constants.error;
                    });
                },
                waysOfReservation: function (waysOfReservationService, constants) {
                    return waysOfReservationService.readAll().catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.editReservation', {
            url: 'reservations/edit/:reservationId',
            controller: require('./../reservations/EditReservationCtrl.js'),
            template: require('../reservations/app.static.editReservation.html'),
            resolve: {
                reservation: function (reservationsService, constants, $stateParams) {
                    return reservationsService.readOne($stateParams.reservationId).catch(function () {
                        return constants.error;
                    });
                },
                guests: function (guestsService, constants) {
                    return guestsService.readAll().catch(function () {
                        return constants.error;
                    });
                },
                rooms: function (roomsService, constants) {
                    return roomsService.readAll().catch(function () {
                        return constants.error;
                    });
                },
                waysOfReservation: function (waysOfReservationService, constants) {
                    return waysOfReservationService.readAll().catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.accommodation', {
            url: '/accommodation',
            controller: require('./../accommodation/AccommodationCtrl.js'),
            template: require('../accommodation/app.static.accommodation.html')
        });

        $stateProvider.state('app.static.createAccommodation', {
            url: 'accommodation/add',
            controller: require('./../accommodation/EditAccommodationCtrl.js'),
            template: require('../accommodation/app.static.editAccommodation.html'),
            resolve: {
                accommodation: function () {
                    return {};
                },
                reservations: function (reservationsService, constants) {
                    return reservationsService.readAllWithoutAccommodationOnTheDay(new Date()).catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $stateProvider.state('app.static.editAccommodation', {
            url: 'accommodation/edit/:accommodationId',
            controller: require('./../accommodation/EditAccommodationCtrl.js'),
            template: require('../accommodation/app.static.editAccommodation.html'),
            resolve: {
                accommodation: function (accommodationService, constants, $stateParams) {
                    return accommodationService.readOne($stateParams.accommodationId).catch(function () {
                        return constants.error;
                    });
                },
                reservations: function (reservationsService, constants, accommodation) {
                    return reservationsService.readAllWithoutAccommodationOnTheDay(accommodation.checkInDate).catch(function () {
                        return constants.error;
                    });
                }
            }
        });

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.interceptors.push('authHelperHttpInterceptor');
    })
    .factory("roomTypesService", require('./../codetables/roomTypesService.js'))
    .factory("RoomTypeDTO", require('./../codetables/RoomTypeDTO.js'))
    .factory("waysOfReservationService", require('./../codetables/waysOfReservationService.js'))
    .factory("WayOfReservationDTO", require('./../codetables/WayOfReservationDTO.js'))
    .factory("reservationsService", require('./../reservations/reservationsService.js'))
    .factory("ReservationDTO", require('./../reservations/ReservationDTO.js'))
    .factory("ReservationForm", require('./../reservations/ReservationForm.js'))
    .factory("roomsService", require('./../rooms/roomsService.js'))
    .factory("RoomDTO", require('./../rooms/RoomDTO.js'))
    .factory("RoomForm", require('./../rooms/RoomForm.js'))
    .factory("accommodationService", require('./../accommodation/accommodationService.js'))
    .factory("AccommodationDTO", require('./../accommodation/AccommodationDTO.js'))
    .factory("AccommodationForm", require('./../accommodation/AccommodationForm.js'))
    .factory("guestsService", require('./../guests/guestsService.js'))
    .factory("GuestDTO", require('./../guests/GuestDTO.js'))
    .factory("UserEditForm", require('./../users/UserEditForm.js'))
    .factory("UserForComboBoxDTO", require('./../users/UserForComboBoxDTO.js'))
    .factory("authHelperHttpInterceptor", require('./../common/interceptors/authHelperHttpInterceptor.js'))
    .factory("httpFacade", require('./../common/services/httpFacade.js'))
    .factory("modalService", require('./../common/modal/modalService.js'))
    .factory("authService", require('./../authentication/authService.js'))
    .factory("formUtilsService", require('./../common/services/formUtilsService.js'))
    .factory("flashService", require('./../common/services/flashService.js'))
    .factory("FlashMessage", require('./../common/utils/FlashMessage.js'))
    .factory("userService", require('./../users/userService.js'))
    .factory("appUtils", require('./../common/services/appUtils.js'))
    .value("constants", require('./../common/services/constants.js'))
    .filter("joinArray", require('./../common/filters/joinArray.js'))
    .directive("datePicker", require('./../common/directives/datePicker.js'))
    .directive('fileUpload', require('./../common/directives/fileUpload.js'))
    .directive('fileInput', require('./../common/directives/fileInput.js'))
    .directive('stSelectMultiple', require('./../common/directives/stSelectMultiple.js'))
    .directive('stSelectMultipleFromArray', require('./../common/directives/stSelectMultipleFromArray.js'))
    .directive('stSelectDistinct', require('./../common/directives/stSelectDistinct.js'))
    .filter('stCustomFilter', require('./../common/filters/stCustomFilter.js'));