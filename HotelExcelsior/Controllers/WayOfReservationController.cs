﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelExcelsior.Models;
using HotelExcelsior.DAO;
using HotelExcelsior.DTO;

namespace HotelExcelsior.Controllers
{
    public class WayOfReservationController : ApiController
    {
        private WayOfReservationService wayOfReservationService = new WayOfReservationService();

        // GET: api/WayOfReservation
        public async Task<List<WayOfReservationDto>> GetWaysOfReservation()
        {
            return await wayOfReservationService.FindAllAsync();
        }

        // GET: api/WayOfReservation/5
        [ResponseType(typeof(WayOfReservationDto))]
        public async Task<IHttpActionResult> GetWayOfReservation(long id)
        {
            WayOfReservationDto wayOfReservation = await wayOfReservationService.FindOneAsync(id);
            if (wayOfReservation == null)
            {
                return NotFound();
            }

            return Ok(wayOfReservation);
        }

        // POST: api/WayOfReservation
        [ResponseType(typeof(WayOfReservationDto))]
        public async Task<IHttpActionResult> PostWayOfReservation(WayOfReservationDto wayOfReservationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            wayOfReservationDto = await wayOfReservationService.SaveNewAsync(wayOfReservationDto);

            return CreatedAtRoute("DefaultApi", new { id = wayOfReservationDto.Id }, wayOfReservationDto);
        }

        // PUT: api/WayOfReservation/5
        [ResponseType(typeof(WayOfReservationDto))]
        public async Task<IHttpActionResult> PutRoomType(long id, WayOfReservationDto wayOfReservationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != wayOfReservationDto.Id)
            {
                return BadRequest();
            }

            wayOfReservationDto = await wayOfReservationService.UpdateAsync(wayOfReservationDto);

            return Ok(wayOfReservationDto);
        }
    }
}