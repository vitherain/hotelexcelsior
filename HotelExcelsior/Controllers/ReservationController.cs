﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelExcelsior.Models;
using HotelExcelsior.DAO;
using HotelExcelsior.DTO;

namespace HotelExcelsior.Controllers
{
    public class ReservationController : ApiController
    {
        private ReservationService reservationService = new ReservationService();

        // GET: api/Reservation
        public async Task<List<ReservationDto>> GetReservations()
        {
            return await reservationService.FindAllAsync();
        }

        // GET: api/Reservation/5
        [ResponseType(typeof(ReservationDto))]
        public async Task<IHttpActionResult> GetReservation(long id)
        {
            ReservationDto reservation = await reservationService.FindOneAsync(id);
            if (reservation == null)
            {
                return NotFound();
            }

            return Ok(reservation);
        }

        // POST: api/Reservation
        [ResponseType(typeof(ReservationDto))]
        public async Task<IHttpActionResult> PostReservation(ReservationForm reservationForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ReservationDto reservationDto = await reservationService.SaveNewAsync(reservationForm);

            return CreatedAtRoute("DefaultApi", new { id = reservationDto.Id }, reservationDto);
        }

        // PUT: api/Reservation/5
        [ResponseType(typeof(ReservationDto))]
        public async Task<IHttpActionResult> PutReservation(long id, ReservationForm reservationForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservationForm.Id)
            {
                return BadRequest();
            }

            ReservationDto reservationDto = await reservationService.UpdateAsync(reservationForm);

            return Ok(reservationDto);
        }

        // DELETE: api/Reservation/5
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteReservation(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            reservationService.Delete(id);

            return StatusCode(HttpStatusCode.OK);
        }
    }
}