﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelExcelsior.Models;
using HotelExcelsior.DAO;
using HotelExcelsior.DTO;
using HotelExcelsior.Services;

namespace HotelExcelsior.Controllers
{
    public class RoomTypeController : ApiController
    {
        private RoomTypeService roomTypeService = new RoomTypeService();

        // GET: api/RoomType
        public async Task<List<RoomTypeDto>> GetRoomTypes()
        {
            return await roomTypeService.FindAllAsync();
        }

        // GET: api/RoomType/5
        [ResponseType(typeof(RoomTypeDto))]
        public async Task<IHttpActionResult> GetRoomType(long id)
        {
            RoomTypeDto wayOfReservation = await roomTypeService.FindOneAsync(id);
            if (wayOfReservation == null)
            {
                return NotFound();
            }

            return Ok(wayOfReservation);
        }

        // POST: api/RoomType
        [ResponseType(typeof(RoomTypeDto))]
        public async Task<IHttpActionResult> PostRoomType(RoomTypeDto roomTypeDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            roomTypeDto = await roomTypeService.SaveNewAsync(roomTypeDto);

            return CreatedAtRoute("DefaultApi", new { id = roomTypeDto.Id }, roomTypeDto);
        }

        // PUT: api/RoomType/5
        [ResponseType(typeof(RoomTypeDto))]
        public async Task<IHttpActionResult> PutRoomType(long id, RoomTypeDto roomTypeDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roomTypeDto.Id)
            {
                return BadRequest();
            }

            roomTypeDto = await roomTypeService.UpdateAsync(roomTypeDto);

            return Ok(roomTypeDto);
        }
    }
}