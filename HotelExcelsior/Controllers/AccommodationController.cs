﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelExcelsior.Models;
using HotelExcelsior.DAO;
using HotelExcelsior.DTO;

namespace HotelExcelsior.Controllers
{
    public class AccommodationController : ApiController
    {
        private AccommodationService accommodationService = new AccommodationService();

        // GET: api/Accommodation
        public async Task<List<AccommodationDto>> GetAccommodations()
        {
            return await accommodationService.FindAllAsync();
        }

        // GET: api/Accommodation/5
        [ResponseType(typeof(AccommodationDto))]
        public async Task<IHttpActionResult> GetAccommodation(long id)
        {
            AccommodationDto wayOfAccommodation = await accommodationService.FindOneAsync(id);
            if (wayOfAccommodation == null)
            {
                return NotFound();
            }

            return Ok(wayOfAccommodation);
        }

        // POST: api/Accommodation
        [ResponseType(typeof(AccommodationDto))]
        public async Task<IHttpActionResult> PostAccommodation(AccommodationForm accommodationForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AccommodationDto accommodationDto = await accommodationService.SaveNewAsync(accommodationForm);

            return CreatedAtRoute("DefaultApi", new { id = accommodationDto.Id }, accommodationDto);
        }

        // PUT: api/Accommodation/5
        [ResponseType(typeof(AccommodationDto))]
        public async Task<IHttpActionResult> PutAccommodation(long id, AccommodationForm accommodationForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accommodationForm.Id)
            {
                return BadRequest();
            }

            AccommodationDto accommodationDto = await accommodationService.UpdateAsync(accommodationForm);

            return Ok(accommodationDto);
        }
    }
}