﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelExcelsior.Models;
using HotelExcelsior.DAO;
using HotelExcelsior.DTO;

namespace HotelExcelsior.Controllers
{
    public class GuestController : ApiController
    {
        private GuestService guestService = new GuestService();

        // GET: api/Guest
        public async Task<List<GuestDto>> GetGuests()
        {
            return await guestService.FindAllAsync();
        }

        // GET: api/Guest/5
        [ResponseType(typeof(GuestDto))]
        public async Task<IHttpActionResult> GetGuest(long id)
        {
            GuestDto wayOfReservation = await guestService.FindOneAsync(id);
            if (wayOfReservation == null)
            {
                return NotFound();
            }

            return Ok(wayOfReservation);
        }

        // POST: api/Guest
        [ResponseType(typeof(GuestDto))]
        public async Task<IHttpActionResult> PostGuest(GuestDto guestDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            guestDto = await guestService.SaveNewAsync(guestDto);

            return CreatedAtRoute("DefaultApi", new { id = guestDto.Id }, guestDto);
        }

        // PUT: api/Guest/5
        [ResponseType(typeof(GuestDto))]
        public async Task<IHttpActionResult> PutGuest(long id, GuestDto guestDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != guestDto.Id)
            {
                return BadRequest();
            }

            guestDto = await guestService.UpdateAsync(guestDto);

            return Ok(guestDto);
        }
    }
}