﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HotelExcelsior.Models;
using HotelExcelsior.DAO;
using HotelExcelsior.DTO;

namespace HotelExcelsior.Controllers
{
    public class RoomController : ApiController
    {
        private RoomService roomService = new RoomService();

        // GET: api/Room
        public async Task<List<RoomDto>> GetRooms()
        {
            return await roomService.FindAllAsync();
        }

        // GET: api/Room/5
        [ResponseType(typeof(RoomDto))]
        public async Task<IHttpActionResult> GetRoom(long id)
        {
            RoomDto wayOfReservation = await roomService.FindOneAsync(id);
            if (wayOfReservation == null)
            {
                return NotFound();
            }

            return Ok(wayOfReservation);
        }

        // POST: api/Room
        [ResponseType(typeof(RoomDto))]
        public async Task<IHttpActionResult> PostRoom(RoomForm roomForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoomDto roomDto = await roomService.SaveNewAsync(roomForm);

            return CreatedAtRoute("DefaultApi", new { id = roomDto.Id }, roomDto);
        }

        // PUT: api/Room/5
        [ResponseType(typeof(RoomDto))]
        public async Task<IHttpActionResult> PutRoom(long id, RoomForm roomForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roomForm.Id)
            {
                return BadRequest();
            }

            RoomDto roomDto = await roomService.UpdateAsync(roomForm);

            return Ok(roomDto);
        }
    }
}