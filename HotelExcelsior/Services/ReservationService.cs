﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class ReservationService
    {
        private ReservationDao reservationDao = new ReservationDao();
        private GuestDao guestDao = new GuestDao();
        private RoomDao roomDao = new RoomDao();
        private WayOfReservationDao wayOfReservationDao = new WayOfReservationDao();

        public List<ReservationDto> FindAll()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<Reservation> reservations = reservationDao.FindAll(context);

                return reservations.Select(x => new ReservationDto
                {
                    Id = x.id,
                    DateIn = x.dateIn,
                    DateOut = x.dateOut,
                    HasAccommodation = x.Accommodation != null && x.Accommodation.Count > 0,
                    Guest = new GuestDto
                    {
                        Id = x.Guest.id,
                        FirstName = x.Guest.firstName,
                        LastName = x.Guest.lastName,
                        Address = x.Guest.address,
                        IdentityCardNumber = x.Guest.identityCardNumber
                    },
                    Room = new RoomDto
                    {
                        Id = x.Room.id,
                        Name = x.Room.name,
                        SmokingAllowed = x.Room.smokingAllowed,
                        RoomType = new RoomTypeDto
                        {
                            Id = x.Room.RoomType.id,
                            Description = x.Room.RoomType.description,
                            SingleBedsCount = x.Room.RoomType.singleBedsCount,
                            DoubleBedsCount = x.Room.RoomType.doubleBedsCount
                        }
                    },
                    WayOfReservation = new WayOfReservationDto
                    {
                        Id = x.WayOfReservation.id,
                        Name = x.WayOfReservation.name
                    }
                }).ToList();
            }
        }

        public async Task<List<ReservationDto>> FindAllAsync()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<Reservation> reservations = await reservationDao.FindAllAsync(context);

                return reservations.Select(x => new ReservationDto
                {
                    Id = x.id,
                    DateIn = x.dateIn,
                    DateOut = x.dateOut,
                    HasAccommodation = x.Accommodation != null && x.Accommodation.Count > 0,
                    Guest = new GuestDto
                    {
                        Id = x.Guest.id,
                        FirstName = x.Guest.firstName,
                        LastName = x.Guest.lastName,
                        Address = x.Guest.address,
                        IdentityCardNumber = x.Guest.identityCardNumber
                    },
                    Room = new RoomDto
                    {
                        Id = x.Room.id,
                        Name = x.Room.name,
                        SmokingAllowed = x.Room.smokingAllowed,
                        RoomType = new RoomTypeDto 
                        {
                            Id = x.Room.RoomType.id,
                            Description = x.Room.RoomType.description,
                            SingleBedsCount = x.Room.RoomType.singleBedsCount,
                            DoubleBedsCount = x.Room.RoomType.doubleBedsCount
                        }
                    },
                    WayOfReservation = new WayOfReservationDto
                    {
                        Id = x.WayOfReservation.id,
                        Name = x.WayOfReservation.name
                    }
                }).ToList();
            }
        }

        public ReservationDto FindOne(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Reservation reservation = reservationDao.FindOne(id, context);

                return new ReservationDto
                {
                    Id = reservation.id,
                    DateIn = reservation.dateIn,
                    DateOut = reservation.dateOut,
                    HasAccommodation = reservation.Accommodation != null && reservation.Accommodation.Count > 0,
                    Guest = new GuestDto
                    {
                        Id = reservation.Guest.id,
                        FirstName = reservation.Guest.firstName,
                        LastName = reservation.Guest.lastName,
                        Address = reservation.Guest.address,
                        IdentityCardNumber = reservation.Guest.identityCardNumber
                    },
                    Room = new RoomDto
                    {
                        Id = reservation.Room.id,
                        Name = reservation.Room.name,
                        SmokingAllowed = reservation.Room.smokingAllowed,
                        RoomType = new RoomTypeDto
                        {
                            Id = reservation.Room.RoomType.id,
                            Description = reservation.Room.RoomType.description,
                            SingleBedsCount = reservation.Room.RoomType.singleBedsCount,
                            DoubleBedsCount = reservation.Room.RoomType.doubleBedsCount
                        }
                    },
                    WayOfReservation = new WayOfReservationDto
                    {
                        Id = reservation.WayOfReservation.id,
                        Name = reservation.WayOfReservation.name
                    }
                };
            }
        }

        public async Task<ReservationDto> FindOneAsync(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Reservation reservation = await reservationDao.FindOneAsync(id, context);

                return new ReservationDto
                    {
                        Id = reservation.id,
                        DateIn = reservation.dateIn,
                        DateOut = reservation.dateOut,
                        HasAccommodation = reservation.Accommodation != null && reservation.Accommodation.Count > 0,
                        Guest = new GuestDto
                        {
                            Id = reservation.Guest.id,
                            FirstName = reservation.Guest.firstName,
                            LastName = reservation.Guest.lastName,
                            Address = reservation.Guest.address,
                            IdentityCardNumber = reservation.Guest.identityCardNumber
                        },
                        Room = new RoomDto
                        {
                            Id = reservation.Room.id,
                            Name = reservation.Room.name,
                            SmokingAllowed = reservation.Room.smokingAllowed,
                            RoomType = new RoomTypeDto
                            {
                                Id = reservation.Room.RoomType.id,
                                Description = reservation.Room.RoomType.description,
                                SingleBedsCount = reservation.Room.RoomType.singleBedsCount,
                                DoubleBedsCount = reservation.Room.RoomType.doubleBedsCount
                            }
                        },
                        WayOfReservation = new WayOfReservationDto
                        {
                            Id = reservation.WayOfReservation.id,
                            Name = reservation.WayOfReservation.name
                        }
                    };
            }
        }

        public async Task<ReservationDto> SaveNewAsync(ReservationForm reservationForm)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Reservation reservationEntity = new Reservation
                {
                    dateIn = reservationForm.DateIn,
                    dateOut = reservationForm.DateOut,
                    Guest = await guestDao.FindOneAsync(reservationForm.GuestId, context),
                    Room = await roomDao.FindOneAsync(reservationForm.RoomId, context),
                    WayOfReservation = await wayOfReservationDao.FindOneAsync(reservationForm.WayOfReservationId, context)
                };

                reservationEntity = await reservationDao.SaveNewAsync(reservationEntity, context);

                return new ReservationDto
                {
                    Id = reservationEntity.id,
                    DateIn = reservationEntity.dateIn,
                    DateOut = reservationEntity.dateOut,
                    HasAccommodation = reservationEntity.Accommodation != null && reservationEntity.Accommodation.Count > 0,
                    Guest = new GuestDto
                    {
                        Id = reservationEntity.Guest.id,
                        FirstName = reservationEntity.Guest.firstName,
                        LastName = reservationEntity.Guest.lastName,
                        Address = reservationEntity.Guest.address,
                        IdentityCardNumber = reservationEntity.Guest.identityCardNumber
                    },
                    Room = new RoomDto
                    {
                        Id = reservationEntity.Room.id,
                        Name = reservationEntity.Room.name,
                        SmokingAllowed = reservationEntity.Room.smokingAllowed,
                        RoomType = new RoomTypeDto
                        {
                            Id = reservationEntity.Room.RoomType.id,
                            Description = reservationEntity.Room.RoomType.description,
                            SingleBedsCount = reservationEntity.Room.RoomType.singleBedsCount,
                            DoubleBedsCount = reservationEntity.Room.RoomType.doubleBedsCount
                        }
                    },
                    WayOfReservation = new WayOfReservationDto
                    {
                        Id = reservationEntity.WayOfReservation.id,
                        Name = reservationEntity.WayOfReservation.name
                    }
                };
            }
        }

        public async Task<ReservationDto> UpdateAsync(ReservationForm reservationForm)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Reservation reservationEntity = await reservationDao.FindOneAsync(reservationForm.Id, context);

                if (reservationEntity != null)
                {
                    reservationEntity.dateIn = reservationForm.DateIn;
                    reservationEntity.dateOut = reservationForm.DateOut;
                    reservationEntity.Guest = await guestDao.FindOneAsync(reservationForm.GuestId, context);
                    reservationEntity.Room = await roomDao.FindOneAsync(reservationForm.RoomId, context);
                    reservationEntity.WayOfReservation = await wayOfReservationDao.FindOneAsync(reservationForm.WayOfReservationId, context);
                }

                reservationEntity = await reservationDao.UpdateAsync(reservationEntity, context);

                return new ReservationDto
                {
                    Id = reservationEntity.id,
                    DateIn = reservationEntity.dateIn,
                    DateOut = reservationEntity.dateOut,
                    HasAccommodation = reservationEntity.Accommodation != null && reservationEntity.Accommodation.Count > 0,
                    Guest = new GuestDto
                    {
                        Id = reservationEntity.Guest.id,
                        FirstName = reservationEntity.Guest.firstName,
                        LastName = reservationEntity.Guest.lastName,
                        Address = reservationEntity.Guest.address,
                        IdentityCardNumber = reservationEntity.Guest.identityCardNumber
                    },
                    Room = new RoomDto
                    {
                        Id = reservationEntity.Room.id,
                        Name = reservationEntity.Room.name,
                        SmokingAllowed = reservationEntity.Room.smokingAllowed,
                        RoomType = new RoomTypeDto
                        {
                            Id = reservationEntity.Room.RoomType.id,
                            Description = reservationEntity.Room.RoomType.description,
                            SingleBedsCount = reservationEntity.Room.RoomType.singleBedsCount,
                            DoubleBedsCount = reservationEntity.Room.RoomType.doubleBedsCount
                        }
                    },
                    WayOfReservation = new WayOfReservationDto
                    {
                        Id = reservationEntity.WayOfReservation.id,
                        Name = reservationEntity.WayOfReservation.name
                    }
                };
            }
        }

        public void Delete(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Reservation reservationEntity = reservationDao.FindOne(id, context);

                reservationDao.Delete(reservationEntity, context);
            }
        }
    }
}