﻿using HotelExcelsior.DAO;
using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.Services
{
    public class RoomTypeService
    {
        private RoomTypeDao roomTypeDao = new RoomTypeDao();

        public async Task<List<RoomTypeDto>> FindAllAsync()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<RoomType> rooms = await roomTypeDao.FindAllAsync(context);

                return rooms.Select(x => new RoomTypeDto
                {
                    Id = x.id,
                    Description = x.description,
                    DoubleBedsCount = x.doubleBedsCount,
                    SingleBedsCount = x.singleBedsCount
                }).ToList();
            }
        }

        public async Task<RoomTypeDto> FindOneAsync(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                RoomType roomType = await roomTypeDao.FindOneAsync(id, context);

                return new RoomTypeDto
                    {
                        Id = roomType.id,
                        Description = roomType.description,
                        DoubleBedsCount = roomType.doubleBedsCount,
                        SingleBedsCount = roomType.singleBedsCount
                    };
            }
        }

        public async Task<RoomTypeDto> SaveNewAsync(RoomTypeDto roomTypeDto)
        {
            using (HotelEntities context = new HotelEntities())
            {
                RoomType roomTypeEntity = new RoomType
                {
                    description = roomTypeDto.Description,
                    doubleBedsCount = roomTypeDto.DoubleBedsCount,
                    singleBedsCount = roomTypeDto.SingleBedsCount
                };
                roomTypeEntity = await roomTypeDao.SaveNewAsync(roomTypeEntity, context);

                return new RoomTypeDto
                {
                    Id = roomTypeEntity.id,
                    Description = roomTypeEntity.description,
                    DoubleBedsCount = roomTypeEntity.doubleBedsCount,
                    SingleBedsCount = roomTypeEntity.singleBedsCount
                };
            }
        }

        public async Task<RoomTypeDto> UpdateAsync(RoomTypeDto roomTypeDto)
        {
            using (HotelEntities context = new HotelEntities())
            {
                RoomType roomTypeEntity = await roomTypeDao.FindOneAsync(roomTypeDto.Id, context);

                if (roomTypeEntity != null)
                {
                    roomTypeEntity.description = roomTypeDto.Description;
                    roomTypeEntity.doubleBedsCount = roomTypeDto.DoubleBedsCount;
                    roomTypeEntity.singleBedsCount = roomTypeDto.SingleBedsCount;
                }

                roomTypeEntity = await roomTypeDao.UpdateAsync(roomTypeEntity, context);

                return new RoomTypeDto
                {
                    Id = roomTypeEntity.id,
                    Description = roomTypeEntity.description,
                    DoubleBedsCount = roomTypeEntity.doubleBedsCount,
                    SingleBedsCount = roomTypeEntity.singleBedsCount
                };
            }
        }
    }
}