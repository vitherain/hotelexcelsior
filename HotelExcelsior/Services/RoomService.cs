﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class RoomService
    {
        private RoomDao roomDao = new RoomDao();
        private RoomTypeDao roomTypeDao = new RoomTypeDao();

        public async Task<List<RoomDto>> FindAllAsync()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<Room> rooms = await roomDao.FindAllAsync(context);

                return rooms.Select(x => new RoomDto
                {
                    Id = x.id,
                    Name = x.name,
                    SmokingAllowed = x.smokingAllowed,
                    RoomType = new RoomTypeDto
                        {
                            Id = x.RoomType.id,
                            Description = x.RoomType.description,
                            DoubleBedsCount = x.RoomType.doubleBedsCount,
                            SingleBedsCount = x.RoomType.singleBedsCount
                        }
                }).ToList();
            }
        }

        public async Task<RoomDto> FindOneAsync(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Room room = await roomDao.FindOneAsync(id, context);

                return new RoomDto
                    {
                        Id = room.id,
                        Name = room.name,
                        SmokingAllowed = room.smokingAllowed,
                        RoomType = new RoomTypeDto
                        {
                            Id = room.RoomType.id,
                            Description = room.RoomType.description,
                            DoubleBedsCount = room.RoomType.doubleBedsCount,
                            SingleBedsCount = room.RoomType.singleBedsCount
                        }
                    };
            }
        }

        public async Task<RoomDto> SaveNewAsync(RoomForm roomForm)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Room roomTypeEntity = new Room
                {
                    name = roomForm.Name,
                    smokingAllowed = roomForm.SmokingAllowed,
                    RoomType = await roomTypeDao.FindOneAsync(roomForm.RoomTypeId, context)
                };

                roomTypeEntity = await roomDao.SaveNewAsync(roomTypeEntity, context);

                return new RoomDto
                {
                    Id = roomTypeEntity.id,
                    Name = roomTypeEntity.name,
                    SmokingAllowed = roomTypeEntity.smokingAllowed,
                    RoomType = new RoomTypeDto
                    {
                        Id = roomTypeEntity.RoomType.id,
                        Description = roomTypeEntity.RoomType.description,
                        DoubleBedsCount = roomTypeEntity.RoomType.doubleBedsCount,
                        SingleBedsCount = roomTypeEntity.RoomType.singleBedsCount
                    }
                };
            }
        }

        public async Task<RoomDto> UpdateAsync(RoomForm roomForm)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Room roomEntity = await roomDao.FindOneAsync(roomForm.Id, context);

                if (roomEntity != null)
                {
                    roomEntity.name = roomForm.Name;
                    roomEntity.smokingAllowed = roomForm.SmokingAllowed;
                    roomEntity.RoomType = await roomTypeDao.FindOneAsync(roomForm.RoomTypeId, context);
                }

                roomEntity = await roomDao.UpdateAsync(roomEntity, context);

                return new RoomDto
                {
                    Id = roomEntity.id,
                    Name = roomEntity.name,
                    SmokingAllowed = roomEntity.smokingAllowed,
                    RoomType = new RoomTypeDto
                    {
                        Id = roomEntity.RoomType.id,
                        Description = roomEntity.RoomType.description,
                        DoubleBedsCount = roomEntity.RoomType.doubleBedsCount,
                        SingleBedsCount = roomEntity.RoomType.singleBedsCount
                    }
                };
            }
        }
    }
}