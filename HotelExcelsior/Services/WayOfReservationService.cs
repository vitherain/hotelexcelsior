﻿using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using HotelExcelsior.DTO;

namespace HotelExcelsior.DAO
{
    public class WayOfReservationService
    {
        private WayOfReservationDao wORDao = new WayOfReservationDao();

        public async Task<List<WayOfReservationDto>> FindAllAsync()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<WayOfReservation> waysOfReservation = await wORDao.FindAllAsync(context);

                return waysOfReservation.Select(x => new WayOfReservationDto
                    {
                        Id = x.id,
                        Name = x.name
                    }).ToList();
            }
        }

        public async Task<WayOfReservationDto> FindOneAsync(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                WayOfReservation wOR = await wORDao.FindOneAsync(id, context);

                return new WayOfReservationDto
                    {
                        Id = wOR.id,
                        Name = wOR.name
                    };
            }
        }

        public async Task<WayOfReservationDto> SaveNewAsync(WayOfReservationDto wORDto)
        {
            using (HotelEntities context = new HotelEntities())
            {
                WayOfReservation wOREntity = new WayOfReservation
                {
                    name = wORDto.Name
                };

                wOREntity = await wORDao.SaveNewAsync(wOREntity, context);

                return new WayOfReservationDto
                {
                    Id = wOREntity.id,
                    Name = wOREntity.name
                };
            }
        }

        public async Task<WayOfReservationDto> UpdateAsync(WayOfReservationDto wORDto)
        {
            using (HotelEntities context = new HotelEntities())
            {
                WayOfReservation wOREntity = await wORDao.FindOneAsync(wORDto.Id, context);

                if (wOREntity != null)
                {
                    wOREntity.name = wORDto.Name;
                }

                wOREntity = await wORDao.UpdateAsync(wOREntity, context);

                return new WayOfReservationDto
                {
                    Id = wOREntity.id,
                    Name = wOREntity.name
                };
            }
        }
    }
}