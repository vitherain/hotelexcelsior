﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class AccommodationService
    {
        private AccommodationDao accommodationDao = new AccommodationDao();
        private ReservationDao reservationDao = new ReservationDao();
        private ReservationService reservationService = new ReservationService();

        public async Task<List<AccommodationDto>> FindAllAsync()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<Accommodation> accommodationList = await accommodationDao.FindAllAsync(context);

                return accommodationList.Select(x => new AccommodationDto
                {
                    Id = x.id,
                    CheckInDate = x.checkInDate,
                    CheckOutDate = x.checkOutDate,
                    Reservation = reservationService.FindOne(x.Reservation.id)
                }).ToList();
            }
        }

        public async Task<AccommodationDto> FindOneAsync(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Accommodation accommodation = await accommodationDao.FindOneAsync(id, context);

                return new AccommodationDto
                {
                    Id = accommodation.id,
                    CheckInDate = accommodation.checkInDate,
                    CheckOutDate = accommodation.checkOutDate,
                    Reservation = reservationService.FindOne(accommodation.Reservation.id)
                };
            }
        }

        public async Task<AccommodationDto> SaveNewAsync(AccommodationForm accommodationForm)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Accommodation accommodationEntity = new Accommodation
                {
                    checkInDate = accommodationForm.CheckInDate,
                    checkOutDate = accommodationForm.CheckOutDate,
                    Reservation = await reservationDao.FindOneAsync(accommodationForm.ReservationId, context)
                };

                accommodationEntity = await accommodationDao.SaveNewAsync(accommodationEntity, context);

                return new AccommodationDto
                {
                    Id = accommodationEntity.id,
                    CheckInDate = accommodationEntity.checkInDate,
                    CheckOutDate = accommodationEntity.checkOutDate,
                    Reservation = reservationService.FindOne(accommodationEntity.Reservation.id)
                };
            }
        }

        public async Task<AccommodationDto> UpdateAsync(AccommodationForm accommodationForm)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Accommodation accommodationEntity = await accommodationDao.FindOneAsync(accommodationForm.Id, context);

                if (accommodationEntity != null)
                {
                    accommodationEntity.checkInDate = accommodationForm.CheckInDate;
                    accommodationEntity.checkOutDate = accommodationForm.CheckOutDate;
                    accommodationEntity.Reservation = await reservationDao.FindOneAsync(accommodationForm.ReservationId, context);
                }

                accommodationEntity = await accommodationDao.UpdateAsync(accommodationEntity, context);

                return new AccommodationDto
                {
                    Id = accommodationEntity.id,
                    CheckInDate = accommodationEntity.checkInDate,
                    CheckOutDate = accommodationEntity.checkOutDate,
                    Reservation = reservationService.FindOne(accommodationEntity.Reservation.id)
                };
            }
        }
    }
}