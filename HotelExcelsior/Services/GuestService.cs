﻿using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using HotelExcelsior.DTO;

namespace HotelExcelsior.DAO
{
    public class GuestService
    {
        private GuestDao guestDao = new GuestDao();

        public async Task<List<GuestDto>> FindAllAsync()
        {
            using (HotelEntities context = new HotelEntities())
            {
                List<Guest> guestList = await guestDao.FindAllAsync(context);

                return guestList.Select(x => new GuestDto
                {
                    Id = x.id,
                    FirstName = x.firstName,
                    LastName = x.lastName,
                    IdentityCardNumber = x.identityCardNumber,
                    Address = x.address
                }).ToList();
            }
        }

        public async Task<GuestDto> FindOneAsync(long id)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Guest guest = await guestDao.FindOneAsync(id, context);

                return new GuestDto
                    {
                        Id = guest.id,
                        FirstName = guest.firstName,
                        LastName = guest.lastName,
                        IdentityCardNumber = guest.identityCardNumber,
                        Address = guest.address
                    };
            }
        }

        public async Task<GuestDto> SaveNewAsync(GuestDto guestDto)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Guest guestEntity = new Guest
                {
                    firstName = guestDto.FirstName,
                    lastName = guestDto.LastName,
                    identityCardNumber = guestDto.IdentityCardNumber,
                    address = guestDto.Address
                };

                guestEntity = await guestDao.SaveNewAsync(guestEntity, context);

                return new GuestDto
                {
                    Id = guestEntity.id,
                    FirstName = guestEntity.firstName,
                    LastName = guestEntity.lastName,
                    IdentityCardNumber = guestEntity.identityCardNumber,
                    Address = guestEntity.address
                };
            }
        }

        public async Task<GuestDto> UpdateAsync(GuestDto guestDto)
        {
            using (HotelEntities context = new HotelEntities())
            {
                Guest guestEntity = await guestDao.FindOneAsync(guestDto.Id, context);

                if (guestEntity != null)
                {
                    guestEntity.firstName = guestDto.FirstName;
                    guestEntity.lastName = guestDto.LastName;
                    guestEntity.identityCardNumber = guestDto.IdentityCardNumber;
                    guestEntity.address = guestDto.Address;
                }

                guestEntity = await guestDao.UpdateAsync(guestEntity, context);

                return new GuestDto
                {
                    Id = guestEntity.id,
                    FirstName = guestEntity.firstName,
                    LastName = guestEntity.lastName,
                    IdentityCardNumber = guestEntity.identityCardNumber,
                    Address = guestEntity.address
                };
            }
        }
    }
}