﻿using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using HotelExcelsior.DTO;

namespace HotelExcelsior.DAO
{
    public class WayOfReservationDao : IDao<WayOfReservation>
    {
        public async Task<List<WayOfReservation>> FindAllAsync(HotelEntities context)
        {
            return await context.WayOfReservation.ToListAsync();
        }

        public async Task<WayOfReservation> FindOneAsync(long id, HotelEntities context)
        {
            return await context.WayOfReservation.Where(x => x.id == id).FirstOrDefaultAsync();
        }

        public async Task<WayOfReservation> SaveNewAsync(WayOfReservation wOR, HotelEntities context)
        {
            context.WayOfReservation.Add(wOR);
            await context.SaveChangesAsync();

            return wOR;
        }

        public async Task<WayOfReservation> UpdateAsync(WayOfReservation wOR, HotelEntities context)
        {
            await context.SaveChangesAsync();

            return wOR;
        }
    }
}