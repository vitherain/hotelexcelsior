﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class RoomDao : IDao<Room>
    {
        public async Task<List<Room>> FindAllAsync(HotelEntities context)
        {
            return await context.Room.Include(x => x.RoomType).ToListAsync();
        }

        public async Task<Room> FindOneAsync(long id, HotelEntities context)
        {
            return await context.Room.Include(x => x.RoomType).Where(x => x.id == id).FirstOrDefaultAsync();
        }

        public async Task<Room> SaveNewAsync(Room room, HotelEntities context)
        {
            context.Room.Add(room);
            await context.SaveChangesAsync();

            return room;
        }

        public async Task<Room> UpdateAsync(Room room, HotelEntities context)
        {
            await context.SaveChangesAsync();

            return room;
        }
    }
}