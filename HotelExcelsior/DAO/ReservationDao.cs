﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class ReservationDao : IDao<Reservation>
    {
        public List<Reservation> FindAll(HotelEntities context)
        {
            return context.Reservation
                .Include(x => x.Guest)
                .Include(x => x.Accommodation)
                .Include(x => x.Room)
                .Include(x => x.WayOfReservation)
                .Include(x => x.Accommodation)
                .ToList();
        }

        public async Task<List<Reservation>> FindAllAsync(HotelEntities context)
        {
            return await context.Reservation
                .Include(x => x.Guest)
                .Include(x => x.Accommodation)
                .Include(x => x.Room)
                .Include(x => x.WayOfReservation)
                .Include(x => x.Accommodation)
                .ToListAsync();
        }

        public Reservation FindOne(long id, HotelEntities context)
        {
            return context.Reservation
                .Include(x => x.Guest)
                .Include(x => x.Accommodation)
                .Include(x => x.Room)
                .Include(x => x.WayOfReservation)
                .Include(x => x.Accommodation)
                .Where(x => x.id == id)
                .FirstOrDefault();
        }

        public async Task<Reservation> FindOneAsync(long id, HotelEntities context)
        {
            return await context.Reservation
                .Include(x => x.Guest)
                .Include(x => x.Accommodation)
                .Include(x => x.Room)
                .Include(x => x.WayOfReservation)
                .Include(x => x.Accommodation)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<Reservation> SaveNewAsync(Reservation reservation, HotelEntities context)
        {
            context.Reservation.Add(reservation);
            await context.SaveChangesAsync();

            return reservation;
        }

        public async Task<Reservation> UpdateAsync(Reservation reservation, HotelEntities context)
        {
            await context.SaveChangesAsync();

            return reservation;
        }

        public void Delete(Reservation reservation, HotelEntities context)
        {
            context.Reservation.Remove(reservation);

            context.SaveChanges();
        }
    }
}