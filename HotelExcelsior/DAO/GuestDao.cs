﻿using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using HotelExcelsior.DTO;

namespace HotelExcelsior.DAO
{
    public class GuestDao : IDao<Guest>
    {
        public async Task<List<Guest>> FindAllAsync(HotelEntities context)
        {
            return await context.Guest.ToListAsync();
        }

        public async Task<Guest> FindOneAsync(long id, HotelEntities context)
        {
            return await context.Guest.Where(x => x.id == id).FirstOrDefaultAsync();
        }

        public async Task<Guest> SaveNewAsync(Guest guest, HotelEntities context)
        {
            context.Guest.Add(guest);
            await context.SaveChangesAsync();

            return guest;
        }

        public async Task<Guest> UpdateAsync(Guest guest, HotelEntities context)
        {
            await context.SaveChangesAsync();

            return guest;
        }
    }
}