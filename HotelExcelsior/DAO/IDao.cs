﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelExcelsior.DAO
{
    public interface IDao<T>
    {
        Task<List<T>> FindAllAsync(HotelEntities context);
        Task<T> FindOneAsync(long id, HotelEntities context);
        Task<T> SaveNewAsync(T t, HotelEntities context);
        Task<T> UpdateAsync(T t, HotelEntities context);
    }
}
