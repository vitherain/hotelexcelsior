﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class AccommodationDao : IDao<Accommodation>
    {
        public async Task<List<Accommodation>> FindAllAsync(HotelEntities context)
        {
            return await context.Accommodation
                .Include(x => x.Reservation)
                .ToListAsync();
        }

        public async Task<Accommodation> FindOneAsync(long id, HotelEntities context)
        {
            return await context.Accommodation
                .Include(x => x.Reservation)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<Accommodation> SaveNewAsync(Accommodation acc, HotelEntities context)
        {
            context.Accommodation.Add(acc);
            await context.SaveChangesAsync();

            return acc;
        }

        public async Task<Accommodation> UpdateAsync(Accommodation acc, HotelEntities context)
        {
            await context.SaveChangesAsync();

            return acc;
        }
    }
}