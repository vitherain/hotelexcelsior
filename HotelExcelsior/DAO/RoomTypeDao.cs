﻿using HotelExcelsior.DTO;
using HotelExcelsior.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace HotelExcelsior.DAO
{
    public class RoomTypeDao : IDao<RoomType>
    {
        public async Task<List<RoomType>> FindAllAsync(HotelEntities context)
        {
            return await context.RoomType.ToListAsync();
        }

        public async Task<RoomType> FindOneAsync(long id, HotelEntities context)
        {
            return await context.RoomType.Where(x => x.id == id).FirstOrDefaultAsync();
        }

        public async Task<RoomType> SaveNewAsync(RoomType roomType, HotelEntities context)
        {
            context.RoomType.Add(roomType);
            await context.SaveChangesAsync();

            return roomType;
        }

        public async Task<RoomType> UpdateAsync(RoomType roomType, HotelEntities context)
        {
            await context.SaveChangesAsync();

            return roomType;
        }
    }
}